<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

if ( ! $post->post_excerpt ) {
	return;
}

?>
<?php if(has_term('fixed-product','product_cat',$post)) {?>

	<form class="cart" method="post" enctype='multipart/form-data'>

		<div class="fixed-product-price">
			<span><?php echo woocommerce_price($product->get_price_including_tax()); ?></span>
		</div>


	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	 	<div class="quantity-container">

		 	<?php
		 		if ( ! $product->is_sold_individually() ) {
		 			echo '<span class="quantity-label">Quantity:</span>';
		 			woocommerce_quantity_input( array(
		 				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
		 				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product ),
		 				'input_value' => ( isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 )
		 			) );
		 		}

		 		
		 	?>

	 	</div>

	 	

	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />


	 	<button type="submit" data-quantity="1" data-product_id="<?php echo $product->id; ?>"
		    class="button alt ajax_add_to_cart add_to_cart_button product_type_simple">
		    ADD TO CART
		</button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' );

		?>

		
	</form>
	
<?php } ?>
<div itemprop="description" class="short-description">
	<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
</div>

<div class="free-samples-product-page">
	<a href="http://www.blindscity.com.au/sample"><img src="http://www.blindscity.com.au/wp-content/uploads/2016/08/OrderSample_Homepage.png" /></a>
</div>

<div class="product-interest-free-banner">
	Up to 12 Months Interest Free<br/>
  <a href="https://www.blindscity.com.au/buy-now-pay-later-interest-free/">Learn More</a>

</div>



<div class="deduction-note">
<strong>NOTE:</strong> For recess fit, we automatically deduct measurement allowances for you.

</div>

<?php /* sample push */ ?>
