<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$cart_item_id = (isset($_POST['edit_cart_item']) && $_POST['edit_cart_item']) ? $_POST['edit_cart_item'] : 0;
?>
<?php if($cart_item_id):?>
    <?php
    $_products = get_posts(array('post_type' => 'product', 'posts_per_page'=>100)); 
    $products = array();
    foreach($_products as $p){
        $products[] = array('id'=>$p->ID,'title'=>$p->post_title);
    }
    ?>
    <h1 class="product_title entry-title">
	<span class="edit_cart_product_selector_wrap">
    <select class="edit_cart_product_selector" id="edit_cart_product_selector" style="width:100%;font-size: 18px;color: #000;font-weight: bold;">
        <?php foreach($products as $p):?>
            <option value="<?php echo $p['id'];?>" <?php if($p['id']==get_the_ID()):?>selected="selected"<?php endif;?>><?php echo $p['title'];?></option>
        <?php endforeach; ?>
    </select>
	<label for="edit_cart_product_selector" class="edit_cart_product_selector_label">Change blind type here</label>
	</span>
    </h1>
<?php else: ?>
	<?php
	
	global $product_fields;
	
	if (!isset($product_fields['is_ready_made_product']) || !$product_fields['is_ready_made_product']): ?>
    <h1 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h1>
	<?php endif; ?>
<?php endif;?>


