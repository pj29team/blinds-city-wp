<?php
/**
 * Cart Page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' );

$edit_cart_item = (isset($_POST['edit_cart_item']) && $_POST['edit_cart_item'])?$_POST['edit_cart_item']:0;
$edit_cart_new_product = (isset($_POST['edit_cart_new_product']) && $_POST['edit_cart_new_product'])?$_POST['edit_cart_new_product']:0;
?>

<div id="cart-content-loader"><div></div></div>

<?php if($edit_cart_item): ?>
<div id="cart-custom-product-container">
	<?php
        $edit_prod_id = $edit_cart_new_product;
        if(!$edit_prod_id){
            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                if ($edit_cart_item == $cart_item_key) {
                    $edit_prod_id = $cart_item['product_id'];
                    break;
		}
            }
        }

        echo do_shortcode('[cart_custom_product id="'.$edit_prod_id.'"]');
	?>
</div>
<?php endif;?>

<?php
if($edit_cart_item) {echo "<div style='display:none;'>";} // hide cart if edit mode is on. BEGIN
?>

<form id="extended_cart_form" action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">
	<input type="hidden" name="edit_cart_item" value=""/>
        <input type="hidden" name="edit_cart_new_product" value=""/>

	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<div class="shop_table cart clearfix" cellspacing="0">

		<!--
				<thead>
						<tr>
								<th class="product-remove">&nbsp;</th>
								<th class="product-thumbnail">&nbsp;</th>
								<th class="product-name"><?php //_e( 'Product', 'woocommerce' ); ?></th>
								<th class="product-price"><?php //_e( 'Price', 'woocommerce' ); ?></th>
								<th class="product-quantity"><?php //_e( 'Quantity', 'woocommerce' ); ?></th>
								<th class="product-subtotal"><?php //_e( 'Total', 'woocommerce' ); ?></th>
						</tr>
				</thead>
		-->

		<div class="cart_table_wrapper clearfix">
					<?php do_action( 'woocommerce_before_cart_contents' ); ?>

					<?php
					$ind = 0;
					foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
							$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
							$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
							$_is_sample   = isset($cart_item['sample']) && $cart_item['sample'];
							$_is_ready_made = isset($cart_item['is_ready_made']) && $cart_item['is_ready_made'];

						/* ?><pre><?php var_dump($cart_item); ?></pre><?php */
							if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
									?>
									<div class="cart_table_row <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
											<?php if($ind++!=0): ?>
											<div class="divider"></div>
											<?php endif; ?>
											<div class="product-thumbnail">
													<?php
															//$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
															$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

															if ( ! $_product->is_visible() ) {
																	echo $thumbnail;
															} else {
																	printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
															}
													?>
											</div>

											<div class="product-name">
													<?php
															if ( ! $_product->is_visible() ) {
																	echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
															} else {
																	echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
															}

															// Meta data
															echo WC()->cart->get_item_data( $cart_item );

															// Backorder notification
															if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
																	echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
															}
													?>
											</div>

											<!--
											<div class="product-price">
													<?php
															// echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
													?>
											</div>
											-->
											<div class="product-edit product-subtotal product-quantity">
													<div class="left">
                                                        <?php if (!$_is_sample && !$_is_ready_made): ?>
	                                                   <div class="edit">
	                                                        <a href="#" rel="<?php echo $cart_item_key; ?>" class="edit_product button" >edit</a>
	                                                    </div>
                                                        <?php endif; ?>
														<?php
																echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
																		'<a href="%s" class="button" title="%s" data-product_id="%s" data-product_sku="%s">remove</a>',
																		esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
																		__( 'Remove this item', 'woocommerce' ),
																		esc_attr( $product_id ),
																		esc_attr( $_product->get_sku() )
																), $cart_item_key );
														?>
													</div>
													<div class="right">
													<?php
															if ( $_product->is_sold_individually() ) {
																	$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
															} else {
																	$product_quantity = woocommerce_quantity_input( array(
																			'input_name'  => "cart[{$cart_item_key}][qty]",
																			'input_value' => $cart_item['quantity'],
																			'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
																			'min_value'   => '0'
																	), $_product, false );
															}

															echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );

															/* deposit button goes here */


													?>

													<?php
															echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
													?>
													</div>
											</div>
									</div>
									<?php
							}
					}

					do_action( 'woocommerce_cart_contents' );
					?>
					<div class="cart_table_row">
                                                <td colspan="6" class="actions">

                                                        <?php if ( WC()->cart->coupons_enabled() ) { ?>
                                                                <div class="coupon">

                                                                        <label for="coupon_code"><?php _e( 'Coupon', 'woocommerce' ); ?>:</label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />

                                                                        <?php do_action( 'woocommerce_cart_coupon' ); ?>
                                                                </div>
                                                        <?php } ?>

                                                        <input type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />

                                                        <?php do_action( 'woocommerce_cart_actions' ); ?>

                                                        <?php wp_nonce_field( 'woocommerce-cart' ); ?>
                                                </td>
                                        </div>
					<?php do_action( 'woocommerce_after_cart_contents' ); ?>
		</div>
	</div>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>
<div class="cart-installers" style="display: none;">
	<div class="installer-header">Need Help With Installation?</div>
	<div class="installer-body">
		<img src="//www.blindscity.com.au/wp-content/uploads/2016/07/hipages-logo.jpg">
		<p><b>BlindsCity</b> and <b>hipages</b> have partnered to give you access to 78,000 verified tradies who are eager to quote for your job</p>
		<a href="http://www.homeimprovementpages.com.au/find/blinds/get_quotes_blinds_city" target="_blank">Find An Installer</a>
	</div>
</div>
<div class="cart-collaterals clearfix ">
	<?php do_action( 'woocommerce_cart_collaterals' ); ?>
</div>

<?php
if($edit_cart_item) {echo "</div>";} // hide cart if edit mode is on. END
?>

<?php do_action( 'woocommerce_after_cart' ); ?>
