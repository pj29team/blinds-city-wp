<?php 
global $product, $post;
$_product = $product; 
$link = get_permalink($post->ID);
$args  = array('postid' => $post->ID, 'width' => 340,'hide_href' => true, 'exclude_video' => false, 'imglink' => false, 'imgnocontainer' => true, 'resizer' => '660auto');
$video = get_obox_media($args);
$image_option = get_option( "ocmx_product_image" ); ?>

<?php

// Ready Made
global $product_fields;

$product_fields = get_fields();

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
	<?php do_action( 'woocommerce_before_single_product', $post, $_product ); ?>
	
	<?php $terms = get_the_terms($post->ID, 'product_cat'); ?>
	<!--Show Breadcrumbs -->
	<ul id="crumbs">
		<li><a href="<?php bloginfo('url'); ?>"><?php _e('Home','ocmx')?></a></li>
		<?php if( !empty( $terms ) ) : ?>
			<li> / </li>
			<li>
				<?php if( is_array( $terms ) ){
				
						//$first_term = array_shift( $terms );

						foreach ($terms as $product_cat_term ) {
							if( $product_cat_term -> slug != 'dont-show-on-homepage' && $product_cat_term-> slug != 'fixed-product'){
								
								break;
							}
						}

						//print_r($terms);

					}

				 ?>

				<a href="<?php echo get_term_link($product_cat_term->slug, "product_cat"); ?>"><?php echo $product_cat_term->name; ?></a>
			</li>
		<?php endif; ?>
		<li> / </li>
		<li><span class="current"><?php the_title(); ?></span></li>
	</ul>
	
	
	<ul class="product-header clearfix">
	
		<li class="gallery-slider <?php if($image_option == "gallery") : ?>gallery-option <?php else: ?>slider-option<?php endif; ?> clearfix">

			<?php if(isset($image_option) && $image_option == "slider") : ?>
				<ul class="gallery-container gallery-image clearfix">
					<?php $attach_args = array("post_type" => "attachment", "post_parent" => $post->ID, "numberposts" => "-1", "orderby" => "menu_order", "order" => "ASC");
					$attachments = get_posts($attach_args);
					if ( !empty($attachments) ) :
	 
						foreach($attachments as $attachement => $this_attachment) :  
							$image = wp_get_attachment_image_src($this_attachment->ID, "660auto");
							$full = wp_get_attachment_image_src($this_attachment->ID,  "full"); ?>
							<li>
								<img src="<?php echo $image[0]; ?>" alt="<?php echo $this_attachment->post_title; ?>" />
							</li>
						<?php endforeach;
	 
					else : ?>
						<li>
							<?php echo $video; ?>
						</li>
					<?php endif; ?>
				</ul>
			
				<?php 
				$counter = count($attachments);
				if( !empty($attachments) && $counter !='1' ) : ?>
	 
					<div class="controls"> 
						<a href="#" class="next"></a> 
						<a href="#" class="previous"></a>
					</div>
				<?php  endif;  ?>

			<?php else : ?>

				<?php do_action( 'woocommerce_before_single_product_summary', $post, $_product ); ?>

			<?php endif; ?>

			<?php 

			echo "<div class='product-custom-content'>". apply_filters('the_content', $post->post_content ) ."</div>";

			?>		

		</li><!--End Slider container -->
		
		<li class="product-content">
			<?php do_action('woocommerce_single_product_summary'); ?>
			<?php if( get_option("ocmx_meta_social") != "false" ) : ?>
				<ul class="social">
					<li class="addthis">
						<!-- AddThis Button BEGIN -->
						<div class="addthis_toolbox addthis_default_style ">
							<a class="addthis_button_facebook_like"></a>
							<a class="addthis_button_tweet"></a>
							<a class="addthis_counter addthis_pill_style"></a>
						</div>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-507462e4620a0fff"></script>
						<!-- AddThis Button END -->
					</li>
				</ul>
			<?php endif; ?>


			<?php 
			if(has_term('simple-product','product_cat',$post)):
				
				$the_product = wc_get_product($post->ID);
				echo 'This is a simple product';


				echo 'price 1 = '.wc_price($product->get_price_including_tax(1,$product->get_price()));
				echo '<br/>price 2 = '.wc_price($the_product->get_price_including_tax(1,$the_product->get_price()));
				echo '<br/>price 3='.$the_product ->get_price();

				echo '<br/>'. do_shortcode('[add_to_cart id="'.$post->ID.'"]');

			endif;

			?>

			<br><br>

			<?php 

			echo "<div class='m-product-custom-content'>". apply_filters('the_content', $post->post_content ) ."</div>";

			?>

			<script type="text/javascript">
			jQuery(document).ready(function(){
				if (jQuery(window).width() > 600) {
					jQuery('.m-product-custom-content').hide();
				} else { 
					jQuery('.product-custom-content').hide();
				}
				jQuery(window).on('resize', function(e) {
					if (jQuery(window).width() > 600) { 
						jQuery('.m-product-custom-content').hide();
						jQuery('.product-custom-content').show();
					} else {
						jQuery('.product-custom-content').hide();
						jQuery('.m-product-custom-content').show();
					}
				});
			});	
			</script>

		</li>
		
	</ul>
	
	<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>	
		<div class="woocommerce_tabs clearfix">

		<?php 
		/*
		   $the_prod_cat = get_the_category($post->ID);
		   echo 'test1';
		   if('draft'==get_post_status($post->ID)):
		   	echo 'test2';
		   print_r($the_prod_cat);

		    do_action( 'woocommerce_after_single_product_summary', $post, isset($_product) ); 
		    echo 'test3';
		    endif;
		    */
		    ?>
		</div>
	</div>
	
	 <!-- Show Upsells or other After Product Stuff -->  


</div>



<style type="text/css">



    #sticky-tip {
        list-style:none;
        margin:0;
        position:fixed;
        right:0px;
        top:340px;
        z-index: 3000;
        background-color: #FE5252;
        padding:20px;
        cursor: pointer;
    }
    .sticky-tip-head {
        color: #fff;
        font-size: 14px;
        font-weight: bold;
        text-align: center;
    }   
    #sticky-tip-content {
        display: none;
        margin: 0;
        position: fixed;
        right: -200px;
        top:0px;
        z-index: 3000;
        border: 5px solid #1bbed1;
        padding: 20px;
        width:470px;
        background: white;
        min-height: 100%;
        z-index: 3000;
    }
    #sticky-tip-close {
        position: absolute;
        right: 0px;
        background: #1bbed1;
        padding: 10px;
        color: white;
        margin: -20px 0px 0px 0px;
        width: 43px;
        text-align: center;
        font-size: 20px;
        cursor: pointer;
    }

    @media only screen and (min-device-width : 375px) and (max-device-width : 667px) { 
        #sticky-tip {
            top: 70%;
            right: 3%;
            bottom: 20%;
            padding-top: 15px;
            width: 18%;
            border-radius: 12px;
        }
        .sticky-tip-head > .glyphicon {
            font-size: 26px;
        }
        .sticky-tip-head > h1 {
            font-size: 11px;
        }
        #sticky-tip-content {
            top: 10%;
            width: 100%;
            height: 100%;
        }
    }
    
    .sticky-inner-content {
	margin-top: 44px;
	overflow: scroll;
    }
    
    .youtube-title {
    	margin-bottom: 20px;
    	font-size: 16px;
    	line-height: 24px;
    	padding: 10px;
    	color: white;
    	text-decoration: underline;
    	text-align: center;
    }

   .youtube-item {
   	 background: #5e7e82;
   }

</style>
 
<?php
$apikey = 'AIzaSyD5q5T3Oizhr1rq_aQrHyJ0upb9_MsiS48';

function renderYoutube($videoid) {
	$json = @file_get_contents('https://www.googleapis.com/youtube/v3/videos?id='.$videoid.'&key='.$apikey.'&part=snippet');
	$ytdata = json_decode($json);
	echo '<h1 class="youtube-title">'. $ytdata->items[0]->snippet->title . '</h1>';
	echo '<p class="youtube-description">' . $ytdata->items[0]->snippet->description .'</p>';
}

$tips = array();
$custom = get_post_custom($post->ID);
foreach($custom as $key => $value) {
	if($key == 'tip_video') {
		$tips[] = $value;
	}
} 
?>
<?php 
if(!empty($tips)) { ?>
<div id="sticky-tip">
    <div class="sticky-tip-head">
        <div class="glyphicon glyphicon-star"></div>
        <h1 class="sticky-tip-head">TIPS</h1>
    </div>
</div> 
<div id="sticky-tip-content">
    <div id="sticky-tip-close">X</div>
	<div class="sticky-inner-content">
   	 <?php 
		foreach($tips as $video) {
			foreach($video as $v) {
				echo '<div class="youtube-item">';
				echo '<iframe width="420" height="315" src="https://www.youtube.com/embed/'.$v.'"></iframe>';
				$json = @file_get_contents('https://www.googleapis.com/youtube/v3/videos?id='.$v.'&key='.$apikey.'&part=snippet');
       				$ytdata = json_decode($json);
        			echo '<h1 class="youtube-title">'. $ytdata->items[0]->snippet->title . '</h1>';
        	//		echo '<p class="youtube-description">' . $ytdata->items[0]->snippet->description .'</p>';
				echo '</div>';
			}
		}
   	 ?>
	</div>
</div>
<?php } ?>
<script type="text/javascript">
    jQuery('#sticky-tip').on('click', function(event) {
        event.preventDefault();
        jQuery("#sticky-tip-content").css('display','block').animate({right: '0px'});
	var window_h = jQuery(window).height() - 100;
	jQuery(".sticky-inner-content").css('height', window_h + 'px');
    });

    jQuery('#sticky-tip-close').on('click', function(event) {
        event.preventDefault();
        jQuery("#sticky-tip-content").animate({right: '-50%'}, function(){
            jQuery("#sticky-tip-content").css({display: 'none'});
        })
    });
</script>
