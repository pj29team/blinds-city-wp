<style type="text/css">
	.blog-main-post-container li.post img {
		margin: 0px !important;
		padding: 0px !important;
	}
</style>
<?php 
global $image_class;
$link = get_permalink($post->ID); 
$args  = array( 'postid' => $post->ID, 'width' => 980, 'height' => 535, 'hide_href' => true, 'imglink' => false, 'imgnocontainer' => true, 'resizer' => 'large', 'exclude_video' => false );
$image = get_obox_media($args);
?>
<li id="post-<?php the_ID(); ?>" <?php post_class('post clearfix'); ?>>		
	<div class="content clearfix" style="margin:50px auto;">
		<!--Begin Top Meta -->
		<div class="post-title-block">
			<!--Show the Title -->
			<h2 class="post-title">
				<a href="<?php echo $link; ?>"><?php the_title(); ?></a>
			</h2>
			<h5 class="date" style="display: none;">
				<?php if( get_option("ocmx_meta_date") != "false" ) {echo the_time(get_option('date_format'));} // Hide the date unless enabled in Theme Options ?>
				<?php if( get_option( "ocmx_meta_author" ) != "false" ) {_e(" written by ", "ocmx"); ?> <?php the_author_posts_link();} //Hide the author unless enabled in Theme Options ?>
				<?php if(get_option( "ocmx_meta_category" ) !="false" ) { _e(" in ",'ocmx'); ?> <?php the_category(", ",'ocmx');} ?> 
			</h5>
		</div>
		<!--Show the Featured Image or Video -->
		<?php if($image != "") : ?> 
			<div class="post-image fitvid"> 
				<?php echo $image; ?>
			</div>
		<?php endif; ?>
		<!--Begin Excerpt -->
		<div class="copy <?php echo $image_class; ?>" style="display: none;">
			<?php if( $post->post_excerpt !== "" ) : ?>
				<?php the_excerpt(); ?>
				<a class="more-info" href="<?php echo $link; ?>"><?php _e("Continue Reading",'ocmx');?></a>
			<?php else :
				the_excerpt("...Continue Reading");
			endif; ?>
		</div>    
	</div>
</li>