<?php 
/* Template Name: Blog */

get_header(); ?>

<ul class="double-cloumn clearfix">
	<li id="left-column">	
		<ul class="blog-main-post-container archive">
			<?php 
			$args = array(
				"post_type" => "post", 
				"paged" => $paged
			);
				
			$temp = $wp_query; 
			$wp_query = null; 
			$wp_query = new WP_Query($args);	
			
			while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
				global $post;
				get_template_part("/functions/fetch-list");
			endwhile; ?> 
		</ul>
		<?php motionpic_pagination("clearfix", "pagination clearfix"); ?>
		<?php $wp_query = null; $wp_query = $temp;?>
	</li>
	<?php get_sidebar(); ?>
</ul>
<?php get_footer(); ?>