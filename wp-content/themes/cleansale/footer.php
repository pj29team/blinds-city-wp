	</div><!--End Content Container -->
	<div id="pre-footer-container">
		<div id="pre-footer" class="clearfix">
		<?php if ( is_active_sidebar( 'pf-left' ) ) : ?>
			<?php dynamic_sidebar( 'pf-left' ); ?>
		<?php endif; ?>
		<?php if ( is_active_sidebar( 'pf-right' ) ) : ?>
			<?php dynamic_sidebar( 'pf-right' ); ?>
		<?php endif; ?>
		</div>
	</div>
	<div id="footer-container">
		<div id="footer" class="clearfix">
			<?php

			$subscribe_form_text = get_option('ocmx_subscribe_form_text');

			?>
			<fieldset id="footer-newsletter">
				<legend>Get the newsletter</legend>
				<p><?php echo $subscribe_form_text; ?></p>
				<form action="//blindscity.us13.list-manage.com/subscribe/post?u=3760e725864a061771776da4a&amp;id=c891e690be" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" id="subForm" class="validate" target="_blank" novalidate>
				<!-- <form id="subForm" method="post" action="http://blindscity.createsend.com/t/i/s/kuxjl/"> -->
					<div class="mc-field-group clearfix">
						<!-- <input type="email" required="" name="cm-kuxjl-kuxjl" id="mce-EMAIL"> -->
						<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
						<button id="mc-embedded-subscribe" type="submit">Sign up</button>
					</div>
				</form>
			</fieldset>
			<fieldset id="footer-confidence">
				<legend>Shop with Confidence</legend>
				<!-- <img src="<?php bloginfo('template_directory'); ?>/images/new/secure.jpg" alt="" class="first"/> -->
				<p>Secure and safe transaction. We accept payments from the following</p>
				<img src="<?php bloginfo('template_directory'); ?>/images/new/cards.jpg" alt=""/>
			</fieldset>
			<fieldset id="footer-links">
				<div id="footer-navigation-container">
					<?php wp_nav_menu(array(
						'menu' => 'Footer Nav',
						'menu_id' => 'footer-nav',
						'menu_class' => 'clearfix',
						'sort_column' 	=> 'menu_order',
						'theme_location' => 'secondary',
						'container' => 'ul',
						'fallback_cb' => 'ocmx_fallback_secondary')
					); ?>
				</div>
				<p><?php echo get_option("ocmx_cleansale_footer"); ?></p>
			</fieldset>
		</div>
		<!--End footer -->
	</div><!--end Footer Container -->
<?php wp_footer(); ?>
<div style="display:none" itemscope itemtype="http://schema.org/LocalBusiness">
<p itemprop="name">Blinds City</p>
<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<p itemprop="streetAddress">84 Hotham St</p>
<p itemprop="addressLocality">Preston</p>
<p itemprop="addressRegion">Victoria</p>
<p itemprop="postalCode">3072</p>
<p itemprop="telephone">1300 055 888</p>
<meta itemprop="latitude" content="-37.746876" />
<meta itemprop="longitude" content="145.008321" />
</div>
</body>
</html>
