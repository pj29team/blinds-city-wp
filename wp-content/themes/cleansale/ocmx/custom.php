<?php 
global $post, $obox_meta, $sliderlink, $maps, $team_meta;

// Custom fields for WP write panel
$obox_meta = array(
		"media" => array (
			"name"			=> __("other_media",'ocmx'),
			"default" 		=> __("", 'ocmx'),
			"label" 		=> __("Image",'ocmx'),
			"desc"      	=> __("Select a feature image to use for your post.",'ocmx'),
			"input_type"  	=> __("image",'ocmx'),
			"input_size"	=> __("50",'ocmx'),
			"img_width"		=> __("535",'ocmx'),
			"img_height"	=> __("255",'ocmx')
		),	
		"video" => array (
			"name"			=> __("video_link",'ocmx'),
			"default" 		=> __("",'ocmx'),
			"label" 		=> __("Video Link",'ocmx'),
			"desc"      	=> __("Provide your video link instead of the embed code and we'll use <a href='http://codex.wordpress.org/Embeds' target='_blank'>oEmbed</a> to translate that into a video.",'ocmx'),
			"input_type"  	=> __("text",'ocmx')
		),	
		"embed" => array (
			"name"			=> __("main_video",'ocmx'),
			"default" 		=> __("",'ocmx'),
			"label" 		=> __("Embed Code",'ocmx'),
			"desc"      	=> __("Input the embed code of your video here.",'ocmx'),
			"input_type"  	=> __("textarea",'ocmx')
		),
		"hostedvideo" => array (
			"name"			=> "video_hosted",
			"default" 		=> "",
			"label" 		=> "Self Hosted Video Formats: .MP4 or .MPV",
			"desc"      	=> "Please paste in your self hosted video link. To upload videos <a href='".get_bloginfo("url")."/wp-admin/media-new.php'>click here</a>",
			"input_type"  	=> "text"
		),
		"hostedvideo_ogv" => array (
			"name"			=> "video_hosted_ogv",
			"default" 		=> "",
			"label" 		=> "Self Hosted Video Formats: .OGV (for non HTML5 browsers)",
			"desc"      	=> "Please paste in your self hosted video link. To upload videos <a href='".get_bloginfo("url")."/wp-admin/media-new.php'>click here</a>",
			"input_type"  	=> "text"
		)
	);
			
$sliderlink = array(
	"name"			=> __("sliderlink",'ocmx'),
	"default" 		=> __("",'ocmx'),
	"label" 		=> __("Slider Link",'ocmx'),
	"desc"      	=> __("Add a link to your slider post",'ocmx'),
	"input_type"  	=> __("text",'ocmx')
);

$maps = array(
	"location" => array (
		"name"		=> "map-location",
		"default"	=> "",
		"label"		=> "Google Maps Location",
		"desc"		=> "Provide a specific address <br /> i.e. (79 Sample Road, Cape Town, Western Cape, South Africa) <br /> <a href='http://maps.google.com' target='_blank'>Check on Google Maps</a> ",
		"input_type"	=> "text"
	),
	"latlong" => array (
		"name"		=> "map-latlong",
		"default"	=> "",
		"label"		=> "Google Maps Latitude & Longitude (Optional)",
		"desc"		=> "Provide specific GPS co-ordinates for Google Maps. Use this if your location does not have a valid Google Maps Location<br /> i.e. (-34.397, 150.644)",
		"input_type"	=> "text"
	),
	"zoom-level" => array (
		"name"		=> "zoom-level",
		"default"	=> "Default",
		"label"		=> "Zoom Level",
		"desc"		=> "Choose the zoom level for your Google map",
		"input_type"	=> "select",
		"options" 	=> array("Close" => 18, "Default" => 15, "Far" => 12)
	),
	"address" => array (
		"name"		=> "address",
		"default"	=> "",
		"label"		=> "Address Shown",
		"desc"		=> "Provide an Address shown to the public.",
		"input_type"	=> "textarea"
	)
);

$team_meta = array(
	"media" => array (
		"name"		=> "other_media",
		"default"	=> "",
		"label"		=> "Image / Logo",
		"desc"		=> "Select a feature image to use for your post.",
		"input_type"	=> "image",
		"input_size"	=> "50",
		"img_width"	=> "535",
		"img_height"	=> "255"
	),
	"position" => array (
		"name"		=> "position",
		"default"	=> "",
		"label"		=> "Position",
		"desc"		=> "eg. CEO, Co-founder.",
		"input_type"	=> "text"
	),
	"facebook" => array (
		"name"		=> "facebook",
		"default"	=> "",
		"label"		=> "Facebook",
		"desc"		=> "eg. http://facebook.com/mark",
		"input_type"	=> "text"
	),
	"twitter" => array (
		"name"		=> "twitter",
		"default"	=> "",
		"label"		=> "Twitter",
		"desc"		=> "eg. http://twitter.com/jack",
		"input_type"	=> "text"
	),
	"linkedin" => array (
		"name"		=> "linkedin",
		"default"	=> "",
		"label"		=> "LinkedIn",
		"desc"		=> "eg. http://www.linkedin.com/profile/view?id=48580712",
		"input_type"	=> "text"
	)
);


function create_meta_box_ui() {
	global $post, $obox_meta, $sliderlink, $maps;
	if(get_post_type($post) == __("slider",'ocmx')) :
		array_unshift($obox_meta, $sliderlink);
	endif;
	if(strpos(get_page_template(), 'contact.php')) :
		$obox_meta = array_merge($maps, $obox_meta);
	endif;
	post_meta_panel($post->ID, $obox_meta);
}

function create_meta_box_ui_team() {
	global $post, $team_meta;
	post_meta_panel($post->ID, $team_meta);
}

function insert_obox_metabox($pID) {
	global $post, $obox_meta, $meta_added, $sliderlink, $maps, $team_meta;
	if(get_post_type($post) == __("slider",'ocmx')) :
		array_unshift($obox_meta, $sliderlink);
	endif;
	if( isset( $post ) && strpos(get_page_template(), 'contact.php')) :
		$obox_meta = array_merge($obox_meta, $maps);
	endif;
	if(!isset($meta_added))
		if(get_post_type() == "team") :
			post_meta_update($post->ID, $team_meta);
		else :
			post_meta_update($pID, $obox_meta, $sliderlink);
		endif;
	$meta_added = 1;
}
if(function_exists("ocmx_change_metatype")) {}

function add_obox_meta_box() {
	if (function_exists('add_meta_box') ) {
		add_meta_box('obox-meta-box',$GLOBALS['themename'].' Options','create_meta_box_ui','post','normal','high');
		add_meta_box('obox-meta-box',$GLOBALS['themename'].' Options','create_meta_box_ui','page','normal','high');
		add_meta_box('obox-meta-box',$GLOBALS['themename'].' Options','create_meta_box_ui','product','normal','high');
		add_meta_box('obox-meta-box',$GLOBALS['themename'].' Options','create_meta_box_ui','portfolio','normal','high');
		add_meta_box('obox-meta-box','Team Options','create_meta_box_ui_team','team','normal','high');
		add_meta_box('obox-meta-box',$GLOBALS['themename'].' Options','create_meta_box_ui','slider','normal','high');
	}
}

function my_page_excerpt_meta_box() {
	add_meta_box( 'postexcerpt', __('Excerpt','ocmx'), 'post_excerpt_meta_box', 'page', 'normal', 'core' );
}

add_action('admin_menu', 'add_obox_meta_box');
add_action('admin_menu', 'my_page_excerpt_meta_box');
add_action('admin_head', 'ocmx_change_metatype');
add_action('save_post', 'insert_obox_metabox');
add_action('publish_post', 'insert_obox_metabox');  ?>