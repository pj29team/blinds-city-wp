<?php /********************/
/* WP COMMENTS      */
function cleansale_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">

                <div class="comment-author vcard comment-avatar">
                    <?php echo get_avatar($comment, $size = '60'); ?>
                </div>
			<div class="comment-meta commentmetadata comment-post clearfix">
				<?php if ($comment->comment_approved == '0') : ?>
					<em><?php _e('Your comment is awaiting moderation.', 'ocmx') ?></em>
					<br />
				<?php endif; ?>
				<h4 class="comment-name"><a href="<?php comment_author_url(); ?>" class="commentor_url" rel="nofollow"><?php comment_author(); ?></a></h4>
				<h5 class="date"><?php comment_date('d M Y h:m a') ?></h5>
				<?php comment_text() ?><?php edit_comment_link(__('(Edit)', 'ocmx'),'  ','') ?>
                <div class="reply reply-to-comment">
					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				</div>
			</div>


	</li>
<?php
}