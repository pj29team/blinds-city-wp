<?php
class ocmx_content_widget extends WP_Widget {
	/** constructor */
	function __construct() {
			$widget_ops = array( 'classname' => 'ocmx_content_widget', 'description' => 'Home Page Widget - Display various kinds of content in a multi-column layout on your home page.' );
			parent::__construct( 'ocmx_content_widget', '(Obox) Content Widget', $widget_ops );
    }
	/** @see WP_Widget::widget */
	function widget($args, $instance) {		
		global $woocommerce;
		
		// Turn $args array into variables.
		extract( $args );
		
		// Turn $instance array into variables
		$instance_defaults = array ( 'excerpt_length' => 80, 'post_thumb' => 1, 'posttype' => 'post', 'postfilter' => '0', 'post_count' => 4, 'layout_columns' => 2);
		$instance_args = wp_parse_args( $instance, $instance_defaults );
		extract( $instance_args, EXTR_SKIP );
		
		// Setup the post filter if it's defined
		if(isset($postfilter) && isset($instance[$postfilter]))
			$filterval = esc_attr($instance[$postfilter]);
		else
			$filterval = 0;	 
		
		if(isset($instance["excerpt_length"]) && $instance["excerpt_length"] != "") :
			$excerpt_length = esc_attr($instance["excerpt_length"]);
		else :
			$excerpt_length = 80;
		endif;

		if(isset($instance["post_category"]))
			$use_category = $instance["post_category"];
		
		if(isset($postfilter) && $postfilter != "" && isset($filterval) && $filterval != "0") :
			$args = array(
				"post_type" => $posttype,
				"posts_per_page" => $post_count,
				"tax_query" => array(
					array(
						"taxonomy" => $postfilter,
						"field" => "slug",
						"terms" => $filterval
					)
				)		
			);
		else :
			$args = array(
				"post_type" => $posttype,
				"posts_per_page" => $post_count
			);
		endif;

		$loop = new WP_Query($args); 
		
		//Set the post Aguments and Query accordingly
		$count = 0;
		$numposts = 0;
		if(isset($filterval) && $filterval !="0")
			if($posttype !='product') :
				$category_id = get_cat_ID( $filterval );
				$category_link = get_category_link( $category_id );
			else :
				$taxonomy = 'product_cat';
				$category_link = get_term_link($filterval, $taxonomy);
			endif;
		
		?>
				
		<div class="content-widget widget clearfix">
		
			<?php if(isset($filterval) && $filterval !='0') : ?>
				<h3 class="widgettitle">
					<a href="<?php echo $category_link; ?>"><?php echo $title; ?></a>
				</h3>
			<?php else: ?>
				<h3 class="widgettitle">
					<span><?php echo $title; ?></span>
				</h3>
			<?php endif; ?>
			
			<ul class="<?php echo $layout_columns; ?>-column clearfix">
			
				<?php if($posttype == "product") : ?>
					<li class="product-container">
						<?php if(isset($filterval) && $filterval !='0') :
							echo do_shortcode('[product_category category="'.$filterval.'" per_page="' . $post_count . '" columns="' . $post_count . '"]');
						else:
							echo do_shortcode('[recent_products per_page="' . $post_count . '" columns="' . $post_count . '"]');
						endif; ?>
					</li>
					
				<?php else : ?>
			
					<?php while ( $loop->have_posts() ) : $loop->the_post();
						global $product;
						$_product = $product;
						if($layout_columns == 'four') : 
							$resizer = '4-3-medium';
							$width = '220';
							$height = '164';
						elseif($layout_columns == 'three') :
							$resizer = '80x80';
							$width = '80';
							$height = '81';
						endif;
						global $post;
						$link = get_permalink($post->ID); 
						$args  = array('postid' => $post->ID, 'width' => $width, 'height' => $height, 'hide_href' => false, 'exclude_video' => $post_thumb, 'imglink' => false, 'imgnocontainer' => true, 'resizer' => $resizer);
						$image = get_obox_media($args); ?>	
						
						<li class="column">
							<?php if($post_thumb != "none") : ?> 
								<div class="post-image fitvid"> 
									<?php echo $image; ?>
								</div>
							<?php endif; ?>
						   
							<?php $content = get_the_content();
							$contenttext = strip_tags($content);
							$excerpt = get_the_excerpt();
							$excerpttext = strip_tags($excerpt); ?>
						   
							<div class="content">
								<?php if(isset($show_date) && $show_date == "on") : ?>
									<h5 class="date"><?php echo date('d M Y', strtotime($post->post_date)); ?></h5>
								<?php endif; ?>
								<h4 class="post-title"><a href="<?php echo $link; ?>"><?php the_title(); ?></a></h4>
								
								<?php if(isset($show_excerpts) && $show_excerpts == "on") : 
									if($post->post_excerpt != "") :
										echo '<p>';
											echo substr($excerpttext, 0, $excerpt_length );
										echo ' ...</p>';
									else :
										echo '<p>';
											echo substr($contenttext, 0, $excerpt_length );
										echo ' ...</p>';
									endif; ?>
								<?php endif; ?>

								<?php if(isset($show_position) && $show_position == "on") : 
									$position = get_post_meta($post->ID, "position", true);
									?>

									<h5 class="position"><?php echo $position; ?></h5>
								<?php endif; ?>

								<?php if(isset($show_social) && $show_social == "on") : 
									$facebook = get_post_meta($post->ID, "facebook", true);
									$twitter = get_post_meta($post->ID, "twitter", true);
									$linkedin = get_post_meta($post->ID, "linkedin", true);
									?>

									<!--Show Social Links -->                   
									<ul class="team-social clearfix">
										<?php if($facebook !='') : ?>
											<li>
												<a class="team-facebook" href="<?php echo $facebook; ?>">Facebook</a>
											</li>
										<?php endif; ?>
										<?php if($twitter !='') : ?>
											<li>
												<a class="team-twitter" href="<?php echo $twitter; ?>">Twitter</a>
											</li>
										<?php endif; ?>
										<?php if($linkedin !='') : ?>
											<li>
												<a class="team-linkedin" href="<?php echo $linkedin; ?>">Linkedin</a>
											</li>
										<?php endif; ?>
									</ul>
								<?php endif; ?>
							</div>
						</li>	
									
					<?php endwhile; ?>
				
				<?php endif; ?>
			</ul>
			
		</div>
	
<?php
	}

	/** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	/** @see WP_Widget::form */
	function form($instance) {
		// Turn $instance array into variables
		$instance_defaults = array ( 'excerpt_length' => 80, 'post_thumb' => 1, 'posttype' => 'post', 'postfilter' => '0', 'post_count' => 4, 'layout_columns' => 2);
		$instance_args = wp_parse_args( $instance, $instance_defaults );
		extract( $instance_args, EXTR_SKIP );
		
		// Setup the post filter if it's defined
		if(isset($postfilter) && isset($instance[$postfilter]))
			$filterval = esc_attr($instance[$postfilter]);
		else
			$filterval = 0;	   

		$post_type_args = array("public" => true, "exclude_from_search" => false, "show_ui" => true);
		$post_types = get_post_types( $post_type_args, "objects");
?>

	<p><em><?php _e("Click Save after selecting a filter from each menu to load the next filter", "ocmx"); ?></em></p>
	
	<p>
		<label for="<?php echo $this->get_field_id('title'); ?>">Title<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php if(isset($title)) echo $title; ?>" /></label>
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id('layout_columns'); ?>">Column Layout</label>
		<select size="1" class="widefat" id="<?php echo $this->get_field_id('layout_columns'); ?>" name="<?php echo $this->get_field_name('layout_columns'); ?>">
				<option <?php if(isset($layout_columns) && $layout_columns == "three") : ?>selected="selected"<?php endif; ?> value="three">3</option>
				<option <?php if(isset($layout_columns) && $layout_columns == "four") : ?>selected="selected"<?php endif; ?> value="four">4</option>
		</select>
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id('posttype'); ?>">Display</label>
		<select size="1" class="widefat" id="<?php echo $this->get_field_id("posttype"); ?>" name="<?php echo $this->get_field_name("posttype"); ?>">
			<option <?php if(isset($posttype) && $posttype == ""){echo "selected=\"selected\"";} ?> value="">--- Select a Content Type ---</option>
				<?php foreach($post_types as $post_type => $details) : ?>
				<option <?php if(isset($posttype) && $posttype == $post_type){echo "selected=\"selected\"";} ?> value="<?php echo $post_type; ?>"><?php echo $details->labels->name; ?></option>
				<?php endforeach; ?>
		</select>
	</p>

	<?php if(isset($posttype) && $posttype != "") :
		if($posttype != "page" && $posttype != "team") : ?>
			<?php $taxonomyargs = array('post_type' => $posttype, "public" => true, "exclude_from_search" => false, "show_ui" => true); 
			$taxonomies = get_object_taxonomies($taxonomyargs,'objects');
			if(!empty($taxonomies)) : ?>
				<p>
					<label for="<?php echo $this->get_field_id('postfilter'); ?>">Filter by</label>
					<select size="1" class="widefat" id="<?php echo $this->get_field_id("postfilter"); ?>" name="<?php echo $this->get_field_name("postfilter"); ?>">
						<option <?php if(isset($postfilter) && $postfilter == ""){echo "selected=\"selected\"";} ?> value="">--- Select a Filter ---</option>
						<?php foreach($taxonomies as $taxonomy => $details) : ?>
							<option <?php if(isset($postfilter) && $postfilter == $taxonomy){echo "selected=\"selected\"";} ?> value="<?php echo $taxonomy; ?>"><?php echo $details->labels->name; ?></option>
						<?php $validtaxes[] = $taxonomy;
						endforeach; ?>
					</select>
				</p>
			<?php endif;
			if(isset($postfilter) && $postfilter != "" && ( (is_array($validtaxes) && in_array($postfilter, $validtaxes)) || !is_array($validtaxes) ) ) :
				$tax = get_taxonomy($postfilter);
				$terms = get_terms($postfilter, "orderby=count&hide_empty=0"); ?>
				<p><label for="<?php echo $this->get_field_id($postfilter); ?>"><?php echo $tax->labels->name; ?></label>
				   <select size="1" class="widefat" id="<?php echo $this->get_field_id($postfilter); ?>" name="<?php echo $this->get_field_name($postfilter); ?>">
						<option <?php if(isset($filterval) && $filterval == 0){echo "selected=\"selected\"";} ?> value="0">All</option>
						<?php foreach($terms as $term => $details) :?>
							<option  <?php if(isset($filterval) && $filterval == $details->slug){echo "selected=\"selected\"";} ?> value="<?php echo $details->slug; ?>"><?php echo $details->name; ?></option>
						<?php endforeach;?>
					</select>
				</p>
			<?php endif; ?>
			<?php if(isset($instance["postfilter"])) : ?>
			
				<p>
					<label for="<?php echo $this->get_field_id('post_count'); ?>">Post Count</label>
					<select size="1" class="widefat" id="<?php echo $this->get_field_id('comment_count'); ?>" name="<?php echo $this->get_field_name('post_count'); ?>">
						<?php $i = 1;
						while($i < 13) :?>
							<option <?php if(isset($post_count) && $post_count == $i) : ?>selected="selected"<?php endif; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php if($i < 1) :
								$i++;
							else: 
								$i=($i+1);
							endif;
						endwhile; ?>
					</select>
				</p>
			<?php endif; ?>
		<?php endif; ?>
		<?php
	endif; ?>
	
	<?php if(isset($posttype) && $posttype != "product" && $posttype != "team") : ?>
		 <p>
			<label for="<?php echo $this->get_field_id('post_thumb'); ?>">Thumbnails</label>
			<select size="1" class="widefat" id="<?php echo $this->get_field_id('post_thumb'); ?>" name="<?php echo $this->get_field_name('post_thumb'); ?>">
					<option <?php if(isset($post_thumb) && $post_thumb == "none") : ?>selected="selected"<?php endif; ?> value="none">None</option>
					<option <?php if(isset($post_thumb) && $post_thumb == "1") : ?>selected="selected"<?php endif; ?> value="1">Post Feature Images</option>
					<?php if($layout_columns != "three") : ?>
						<option <?php if(isset($post_thumb) && $post_thumb == "0") : ?>selected="selected"<?php endif; ?> value="0">Videos</option>
					<?php endif; ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('show_date'); ?>">
				<input type="checkbox" <?php if(isset($show_date) && $show_date == "on") : ?>checked="checked"<?php endif; ?> id="<?php echo $this->get_field_id('show_date'); ?>" name="<?php echo $this->get_field_name('show_date'); ?>">
				Show Date
			</label>
		</p>
	
		<p>
			<label for="<?php echo $this->get_field_id('show_excerpts'); ?>">
				<input type="checkbox" <?php if(isset($show_excerpts) && $show_excerpts == "on") : ?>checked="checked"<?php endif; ?> id="<?php echo $this->get_field_id('show_excerpts'); ?>" name="<?php echo $this->get_field_name('show_excerpts'); ?>">
				Show Excerpts
			</label>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('excerpt_length'); ?>">Excerpt Length (character count)<input class="shortfat" id="<?php echo $this->get_field_id('excerpt_length'); ?>" name="<?php echo $this->get_field_name('excerpt_length'); ?>" type="text" value="<?php if(isset($excerpt_length)) echo $excerpt_length; ?>" /><br /></label>
		</p>
	<?php endif; ?>

	<?php if(isset($posttype) && $posttype == "team") : ?>
		<p>
			<label for="<?php echo $this->get_field_id('show_position'); ?>">
				<input type="checkbox" <?php if(isset($show_position) && $show_position == "on") : ?>checked="checked"<?php endif; ?> id="<?php echo $this->get_field_id('show_position'); ?>" name="<?php echo $this->get_field_name('show_position'); ?>">
				Show Position
			</label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('show_social'); ?>">
				<input type="checkbox" <?php if(isset($show_social) && $show_social == "on") : ?>checked="checked"<?php endif; ?> id="<?php echo $this->get_field_id('show_social'); ?>" name="<?php echo $this->get_field_name('show_social'); ?>">
				Show Social Networks
			</label>
		</p>
	<?php endif; ?>
	

	
<?php 
	} // form

}// class

//This sample widget can then be registered in the widgets_init hook:

// register FooWidget widget
add_action('widgets_init', create_function('', 'return register_widget("ocmx_content_widget");'));

?>