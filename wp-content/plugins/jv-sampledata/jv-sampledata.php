<?php
/*
Plugin Name: JV Data Sample
Plugin URI: http://www.joomlavi.com
Description: It, use import / export data sample
Version: 1.1.0
Author: Joomlavi
Author URI: http://www.joomlavi.com
*/


if( !class_exists( 'jvdatasample' ) ) {
	
	class jvdatasample {
        
        public static $urlExportZip = 'rev_sliders';
        
        public static function pholder() {
            return __DIR__ . '/assets/images/placeholder.png';
        }
		
		public static function itype( $types = array() ) {
			return array(
                'content' 	 => 'Content',
				'page' 		 => 'Page options',
				'widget' 	 => 'Widgets',
				'menu' 		 => 'Menus',
				'customizer' => 'customizer options',
                'revslider'  => 'rev slider'
			);
		}
        
        public static function folder() {
            return 'theme01';
        }

        public static function jvtheme_adv() {
        	wp_register_script( 
        		'jvtheme_adv',  
        		plugins_url( 'assets/js/do.js' , __FILE__ )
        	);

        	// Localize the script with new data
            $export     = apply_filters( 'jvtheme_import_types', array() );
            $export     = array_keys( $export );
			$js_inine   = array(
				'dsource' 	=> apply_filters( 'jvtheme_import_demos', 'theme01' ),
				'sexport'   => $export,
                'msg'       => array(
                    'ask'         => esc_html__( 'Are you want to continue ?', 'jvtheme'),
                    'import'      => esc_html__( 'Import', 'jvtheme' ),
                    'export'      => esc_html__( 'Export', 'jvtheme' ),
                    'download'    => esc_html__( 'Downloading', 'jvtheme' ),
                    'success'     => esc_html__( 'Action is successful!', 'jvtheme' ),
                )
			);
			wp_localize_script( 'jvtheme_adv', 'JV', $js_inine );

			// Enqueued script with localized data.
			wp_enqueue_script( 'jvtheme_adv' );
        }

        public static function getImportDataFolderPath() {
			return get_template_directory() . '/inc/import/files/';
		}
        
        public static function cfolder( $path = '', $include = false ) {
            $spacer = '/';
            $path   = implode(
                $spacer, 
                !$include ? array(
                    rtrim( self::getImportDataFolderPath(), $spacer ), 
                    rtrim( $path, $spacer )
                ) : array( rtrim( $path, $spacer ) )
            );
            
            if( is_dir($path) ) { return false; }
            
            mkdir( $path, 0777, true );
        }      
	}
}
require_once( dirname(__FILE__) . '/import/import.php' );
require_once( dirname(__FILE__) . '/export/export.php' );
require_once( dirname(__FILE__) . '/reset.php' );


add_filter( 'jvtheme_import_types', 'jvdatasample::itype', 10, 1 );
add_filter( 'jvtheme_import_demos', 'jvdatasample::folder', 10 );

add_action( 'jvtheme_adv', 'jvdatasample::jvtheme_adv',10 );
add_action( 'jvtheme_cfolder', 'jvdatasample::cfolder',10, 2 );