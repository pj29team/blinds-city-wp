<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


/**
 * Class for handling express checkout flow
 *
 * @class    WC_ZipMoney_Express
 * @version  1.1.0
 * @package  zipMoney Payments
 * @author   zipMoney Payments
 */
class WC_ZipMoney_Express extends WC_ZipMoney
{
	const EXPRESS_CHECKOUT_ROUTE   = 'zipmoneypayment/expresscheckout/';

  const API_ERROR_CODE_OK                             = 0;
  const API_ERROR_CODE_GENERAL_ERROR                  = 100;
  const API_ERROR_CODE_QUOTE_CHANGED                  = 101;
  const API_ERROR_CODE_ORDER_NOT_IN_REQUEST           = 102;
  const API_ERROR_CODE_NO_ORDER_ITEMS_IN_REQUEST      = 103;

  const QUOTE_ADDRESS_TYPE_SHIPPING					= "shipping";
  const QUOTE_ADDRESS_TYPE_BILLING					= "billing";

  const IFRAME_API_URL_LIVE                 = 'https://account.zipmoney.com.au/scripts/iframe/zipmoney-checkout.js';
  const IFRAME_API_URL_TEST                 = 'https://account.sandbox.zipmoney.com.au/scripts/iframe/zipmoney-checkout.js';
  const IFRAME_API_URL_DEVELOPMENT          = 'http://account.dev1.zipmoney.com.au/scripts/iframe/zipmoney-checkout.js';

  public $checkout_source  = null;

  /**
   * Handles express checkout. Makes a request to /quote endpoint.
   * Returns redirect_url when successful.
   *
   * @access public
   * @return mixed boolean | string $redirect_url
   */
	public function handleExpressCheckout( )
	{
		global $woocommerce;

		$quote  = $this->_create_quote();

		$requestArray = $this->_prepareRequestForCheckout($quote);

		$this->log( "Express Request:- " . json_encode($requestArray) );

		try{

			$response  = $this->_api->quote($requestArray);

			$this->log("Response:- " . json_encode($response->toArray()) );

			if($response->isSuccess())
		    return $response->getRedirectUrl();
		  else
		    return false;

		} catch (Exception $e){
			//Logging
			$this->log($e->getMessage());

			return false;
		}

	}

  /**
   * Makes a request to /quote endpoint through the handleExpressCheckout() method.
   * Prints json response.
   *
   * @access public
   */
  public  function getredirecturl()
  {
    $redirect_url = $this->handleExpressCheckout();
    $json = array('redirect' => $redirect_url,"result"=>"success");
    header("Content-type:application/json");
		$this->log('Ryan:- ' . json_encode($json));
    die(json_encode($json));
  }

  /**
   * Creates a quote using the custom "quote" post type.
   * Returns redirect_url when successful..
   *
   * @access protected
   * @throws Exception
   * @return WC_ZipMoney_Quote $quote object
   */
	protected function _create_quote()
	{
		global $woocommerce,$wp;

		if(isset($wp->query_vars['checkout_source']) && !empty($wp->query_vars['checkout_source']))
		{
			$this->checkout_source = $wp->query_vars['checkout_source'];

			if($wp->query_vars['checkout_source'] == 'product_page')
			{

				if(isset($wp->query_vars['product_id']) && !empty($wp->query_vars['product_id']))
				  $product_id = trim($wp->query_vars['product_id']);
			  else
				  throw new  Exception("Product id not provided", 1);
			}
	 	}


		if ( ! defined( 'WOOCOMMERCE_CART' ) )
		     define( 'WOOCOMMERCE_CART', true );

		$woocommerce->cart->add_to_cart($product_id);

		WC()->cart->calculate_totals();

		$quote_id = $woocommerce->checkout()->create_order();

		// set the post type to quote
		set_post_type($quote_id,'shop_quote');

		$quote    = new WC_ZipMoney_Quote($quote_id);

		// create order object
		if ( is_user_logged_in() ) {
			$current_user = wp_get_current_user();

			//Billing Address
			$billing_address   = array(
										'first_name' => get_user_meta( $current_user->ID, 'billing_first_name', true ),
										'last_name'  => get_user_meta( $current_user->ID, 'billing_last_name', true ),
										'address_1'  => get_user_meta( $current_user->ID, 'billing_address_1', true ),
										'address_2'	 => get_user_meta( $current_user->ID, 'billing_address_2', true ),
										'city'		   => get_user_meta( $current_user->ID, 'billing_city', true ),
										'state'		   => get_user_meta( $current_user->ID, 'billing_state', true ),
										'postcode'	 => get_user_meta( $current_user->ID, 'billing_postcode', true ),
										'phone'	 	   => get_user_meta( $current_user->ID, 'billing_phone', true ),
										'country'	   => get_user_meta( $current_user->ID, 'billing_country', true )
									);
			//Shipping Address
			$shipping_address   = array(
										'first_name' => get_user_meta( $current_user->ID, 'shipping_first_name', true ),
										'last_name'  => get_user_meta( $current_user->ID, 'shipping_last_name', true ),
										'address_1'  => get_user_meta( $current_user->ID, 'shipping_address_1', true ),
										'address_2'  => get_user_meta( $current_user->ID, 'shipping_address_2', true ),
										'city'		   => get_user_meta( $current_user->ID, 'shipping_city', true ),
										'state'	  	 => get_user_meta( $current_user->ID, 'shipping_state', true ),
										'postcode'	 => get_user_meta( $current_user->ID, 'shipping_postcode', true ),
										'phone'	  	 => get_user_meta( $current_user->ID, 'shipping_phone', true ),
										'country'	   => get_user_meta( $current_user->ID, 'shipping_country', true ),
									);

			$quote->set_address( $billing_address,  self::QUOTE_ADDRESS_TYPE_BILLING );

			$quote->set_address( $shipping_address, self::QUOTE_ADDRESS_TYPE_SHIPPING  );
		}

		$quote->set_payment_method( $this->_gateway );

		if(isset($current_user))
			update_post_meta($quote_id, '_customer_user', $current_user->ID);

		return $quote;
	}

  /**
   * Converts quote object(WC_ZipMoney_Quote) to order object(WC_Order).
   *
   * @access public
   */
	public function convert_quote_to_order($quote)
	{

		set_post_type($quote->id,'shop_order');

		$order = new WC_Order($quote->id);

		$order -> update_status('wc-zip-authorised');

		$order -> update_status('processing');
	}

  /**
   * Prepares $requestArray array for quote call
   *
   * @access protected
   * @return array $requestArray
   */
	protected function _prepareRequestForCheckout($quote)
	{
		global $woocommerce;

		$requestArray = array();

		// Returns the checkout object
		$checkout     = $woocommerce->checkout();

		// Set Basic required info
		$requestArray['quote_id']	   =  (string)$quote->id;
		// Set Callback Urls
		$requestArray = $this->_setCallbackUrls($requestArray,$quote);

		// Set the consumer array
		if($consumer = $this->_setCustomerInfo($quote)){
			$requestArray['consumer']     = $consumer;
		}

		if($billing_info = $this->_setBillingInfo($quote)){
			// Set the billing info
			$requestArray['billing_address']  = $this->_setBillingInfo($quote);
			// Set the shipping info
			$requestArray['shipping_address'] = $this->_setBillingInfo($quote);
		}

		// If different shipping address is provided
		if($quote->shipping_address_1)  $requestArray['shipping_address'] =  $this->_setShippingInfo($quote);

		// Set the order info
		$requestArray['order']    = $this->_setOrder($quote);
 		// Set Version Info
		$requestArray['version']  = array( 'client' => self::CLIENT, "platform" => self::PLATFORM );
		// Set Metadata
		$requestArray['metadata'] = array( 'order_reference' => null);

		$requestArray['checkout_source'] = $this->checkout_source;

	return $requestArray;
	}

  /**
   * Prepares $responseArray array for webhook
   *
   * @access public
   * @param  WC_ZipMoney_Quote $quote, array $request, boolean $validate
   * @throws Exception
   * @return array $responseArray
   */
	public function prepareResponseForExpress( $quote, $request ,$validate = true)
	{
		global $woocommerce;

		$responseArray = array();

		// Returns the checkout object
		$checkout     = $woocommerce->checkout();

		// Set Basic required info
		$responseArray['quote_id']	 =  (string)$quote->id;
		// Set Callback Urls
		$responseArray = $this->_setCallbackUrls($responseArray,$quote);

		// Set the consumer array
		if($consumer = $this->_setCustomerInfo($quote)){
			$responseArray['consumer']     = $consumer;
		}

		if($billing_info = $this->_setBillingInfo($quote)){

			// Set the billing info
			$responseArray['billing_address']  = $billing_info;
			// Set the shipping info
			$responseArray['shipping_address'] = $billing_info;
		}

		if($shipping_info = $this->_setShippingInfo($quote,$request))
			$responseArray['shipping_address'] =  $shipping_info;


		// Set the order info
		$responseArray['order']    = $this->_setOrder($quote);
 		// Set Version Info
		$responseArray['version']  = array( 'client' => self::CLIENT, "platform" => self::PLATFORM );
		// Set Metadata
		$responseArray['metadata'] = array( 'order_reference' => null);


		if(!$validate) return $responseArray;

		$ret = $this->validateRequestIntegrity( $quote, $request );

		// Compare Request and Response
		if($ret === null )
			throw new Exception("No data returned");

    $quote_has_changed = isset($ret['changed']) ? $ret['changed'] : null;
    $message = isset($ret['message']) ? $ret['message'] : '';

    $responseArray['error_code'] = self::API_ERROR_CODE_OK;
    $responseArray['message']    = '';

    if ($quote_has_changed) {
      $message = sprintf('The shopping cart details might have been changed. ') . $message;
      $responseArray['error_code'] = self::API_ERROR_CODE_QUOTE_CHANGED;
      $responseArray['message']    = $message;
	    WC_ZipMoney::staticLog("Additional Data Appended in the response:-\n".$message, $this->_gateway->debug);
    }

		return $responseArray;
	}

  /**
   * Sets callback urls to the  $requestArray.
   *
   * @access protected
   * @param  array $requestArray, WC_ZipMoney_Quote $quote
   * @return array $requestArray
   */
	protected function _setCallbackUrls($requestArray, $quote)
	{  global $woocommerce;

		// Set callback Urls
		$requestArray['cancel_url']	  = $quote->get_cancel_order_url();
		$requestArray['error_url']	  = $this->_gateway->get_error_url($quote);
		$requestArray['decline_url']  = $woocommerce->cart->get_cart_url();
		$requestArray['cart_url']	    = $woocommerce->cart->get_cart_url();
		$requestArray['success_url']  = $this->_gateway->get_return_url($quote);
		$requestArray['refer_url']	  = $this->_gateway->get_return_url($quote);

	return $requestArray;
	}

  /**
   * Checks if the webhook request has changed.
   *
   * @access public
   * @param  WC_ZipMoney_Quote $quote, array $request,
   * @return array $ret
   */
	public function validateRequestIntegrity( $quote, $request )
	{
		$ret = array(
            'changed' => false,
            'message' => '',
        );

		$quote_user = $quote->get_user();
		// Compare Customer Info
		if($quote_user  && $request->consumer){

			if ((isset($request->consumer->email) && $request->consumer->email != $quote_user->data->user_email)
	            || (isset($request->consumer->first_name) && $request->consumer->first_name != get_user_meta( $quote_user->ID, 'first_name', true ))
	            || (isset($request->consumer->last_name) && $request->consumer->last_name != get_user_meta( $quote_user->ID, 'last_name', true ))
	        ) {
	            $ret['changed'] = true;
	            $ret['message'] = sprintf('Customer details have changed.');
	            return $ret;
	        }

    	}


      if (isset($request->shipping_address)) {
          if ($this->_compareAddress($quote, $request->shipping_address,'shipping')) {
              $ret['changed'] = true;
              $ret['message'] = sprintf('Shipping address has changed.');
              return $ret;
          }
      }


      if (isset($request->shipping_address->selected_option_id)) {
          // if ($quote->getShippingAddress()->getShippingMethod() != $oRequest->shipping_address->selected_option_id) {
          //     $aReturn['changed'] = true;
          //     $aReturn['message'] = $this->__('Shipping method has been changed from %s to %s.', $oRequest->shipping_address->selected_option_id, $oQuote->getShippingAddress()->getShippingMethod());
          //     return $aReturn;
          // }
      }


      if (isset($request->billing_address)) {
          if ($this->_compareAddress($quote, $request->billing_address,'billing')) {
              $ret['changed'] = true;
              $ret['message'] = sprintf('Billing address has changed.');
              return $ret;
          }
      }


      $ret = $this->_compareQuote($quote, $request);
    return $ret;
	}

  /**
   * Compares address details between request and quote.
   *
   * @access private
   * @param  WC_ZipMoney_Quote $quote, array $request_address, string $address_type
   * @return boolean
   */
	private function _compareAddress($quote, $request_address, $address_type)
    {
    	if($address_type=='billing'){
	        if ((isset($request_address->line1) 	 && $request_address->line1   != $quote->billing_address_1)
	            //|| (isset($request_address->line2) 	 && $request_address->line2   != $quote->billing_address_2)
	            || (isset($request_address->country) && $request_address->country != $quote->billing_country)
	            || (isset($request_address->zip) 	 && $request_address->zip     != $quote->billing_postcode)
	            || (isset($request_address->city) 	 && $request_address->city    != $quote->billing_city)
	            || (isset($request_address->state)   && $request_address->state   != $quote->billing_state)
	        )
	       return true;
	  } else if($address_type == 'shipping') {
	  		if((isset($request_address->line1) 		 && $request_address->line1   != $quote->shipping_address_1)
	            || (isset($request_address->line2) 	 && $request_address->line2   != $quote->shipping_address_2)
	            || (isset($request_address->country) && $request_address->country != $quote->shipping_country)
	            || (isset($request_address->zip) 	 && $request_address->zip     != $quote->shipping_postcode)
	            || (isset($request_address->city) 	 && $request_address->city    != $quote->shipping_city)
	            || (isset($request_address->state)   && $request_address->state   != $quote->shipping_state)
	        )
	       return true;
	  }

    return false;
  }

  /**
   * Compares quote details between webhook request and existing corresponding quote object.
   *
   * @access private
   * @param  WC_ZipMoney_Quote $quote, array $request_address, boolean $check_totals
   * @return boolean
   */
  private function _compareQuote($quote, $request, $check_totals = true )
  {

  	$ret = array(
      'changed' => false,
      'message' => '',
    );

	 // Check if order is empty in request
    if (!isset($request->order)) {

      $ret['changed'] = true;
      $ret['message'] = $this->__('Order missing from request.');

      return $ret;
    }

	 // Check if order id empty in request
    if (isset($request->order->id) && $request->order->id != $quote->id) {
      $ret['changed'] = true;
      $ret['message'] = sprintf('Order id has been changed from %s to %s.', $request->order->id, $quote->id);

      return $ret;
    } else if (!isset($request->order->id)) {
      $ret['changed'] = true;
      $ret['message'] = __('Order id  missing from request.');
      return $ret;
    }

    // Check if order detail is in request
    if (!isset($request->order->detail) || !count($request->order->detail)) {
      $ret['changed'] = true;
      $ret['message'] = sprintf('Order detail  missing from request.');
      return $ret;
    }

    $quote_items = $quote->get_items();
    $quote_item_count  =  count($quote_items);

    // Check if number of items have changed
    if (count($request->order->detail) != $quote_item_count) {

      $ret['changed'] = true;
      $ret['message'] = sprintf('Number of items have been changed from %s to %s.', count($request->order->detail), $quote_item_count);

      return $ret;
    }


    foreach ($request->order->detail as $request_item) {
      $request_item_id = isset($request_item->id) ? $request_item->id : null;

      if ($request_item_id === null) {
          $ret['changed'] = true;
          $ret['message'] = sprintf('Item id missing from request.');
          return $ret;
      }

      $quote_item  = $quote_items[ $request_item_id ] ;
      $product 	 = new WC_Product($quote_item['product_id']);

      $item_final_price = $quote->get_item_total($quote_item);
      $qty = round($quote_item['qty']);

      // Check if item still exists
      if (!$quote_item) {
          $ret['changed'] = true;
          $ret['message'] = sprintf('The item (id:%s) does not exist in quote.', $request_item_id );
          return $ret;
      }

      // Check SKU
      if (isset($request_item->sku) && $request_item->sku != $product->get_sku()) {

          $ret['changed'] = true;
          $ret['message'] = sprintf('The item (id:%s) sku has been changed from %s to %s.', $request_item_id , $request_item->sku, $quote->get_item_meta($quote_item,'sku'));

          return $ret;
      }

      // Check Item Price
      if (isset($request_item->price) && abs($request_item->price - $item_final_price) >= 0.0001) {
          $ret['changed'] = true;
          $ret['message'] = sprintf('The item (id:%s) price has been changed from %s to %s.', $request_item_id, $request_item->price, $item_final_price);

          return $ret;
      }

      // Check Quantity
      if (isset($request_item->quantity) && ($request_item->quantity - $qty) >= 0.0001) {
          $ret['changed'] = true;
          $ret['message'] = sprintf('The item (id:%s) qty has been changed from %s to %s.', $request_item_id, $request_item->quantity, $qty);
          return $ret;
      } else if (!isset($request_item->quantity)) {
          $ret['changed'] = true;
          $ret['message'] = sprintf('The item (id:%s) qty  missing from request.', $request_item_id);
          return $ret;
      }

      // if (isset($oRequestItem->discount_percent) && abs($oRequestItem->discount_percent - $fDiscountPercent) >= 0.0001) {
      //     $aReturn['changed'] = true;
      //     $aReturn['message'] = sprintf('The item (id:%s) discount percent has been changed from %s to %s.', $iItemId, $oRequestItem->discount_percent, $fDiscountPercent);
      //     return $aReturn;
      // }

      // if (isset($oRequestItem->discount_amount) && abs($oRequestItem->discount_amount - $fDiscountAmount) >= 0.0001) {
      //     $aReturn['changed'] = true;
      //     $aReturn['message'] = sprintf('The item (id:%s) discount amount has been changed from %s to %s.', $iItemId, $oRequestItem->discount_amount, $fDiscountAmount);
      //     return $aReturn;
      // }
    }

    // Check for Grand Totals
    if ($check_totals) {
      $quote_shipping_total = $quote->get_total_shipping();
      $quote_grand_total    = (float) $quote->get_total();

      if (isset($request->order->total) && abs($request->order->total - $quote_grand_total) >= 0.0001) {
          $ret['changed'] = true;
          $ret['message'] = sprintf('Grand total has been changed from %s to %s.', $request->order->total, $quote_grand_total);
          return $ret;

      } else if (!isset($request->order->total)) {echo 6;
          $ret['changed'] = true;
          $ret['message'] = sprintf('Order total amount is missing from request.');
          return $ret;
      }

      if (isset($request->order->shipping_value) && abs($request->order->shipping_value - $quote_shipping_total) >= 0.0001) {
          $ret['changed'] = true;
          $ret['message'] = sprintf('Shipping amount has been changed from %s to %s.', $request->order->shipping_value, $quote_shipping_total);
          return $ret;
      }
    }

    return $ret;
  }

  /**
   * Updates the address in the request array.
   *
   * @access public
   * @param  WC_ZipMoney_Quote $quote, array $request_address, string $address_type
   * @return boolean
   */
  public function updateAddress($quote, $request, $address_type)
  {
  	if (!$request || !$quote)
      return false;

    if ($address_type == self::QUOTE_ADDRESS_TYPE_SHIPPING && isset($request->shipping_address))
        $req_address = $request->shipping_address;
    else if ($address_type == self::QUOTE_ADDRESS_TYPE_BILLING && isset($request->billing_address))
    	$req_address = $request->billing_address;

    if (!isset($req_address))
      return false;

    $first_name     = isset($req_address->first_name) ? $req_address->first_name : null;
    $last_name      = isset($req_address->last_name) ? $req_address->last_name : null;
    $email          = isset($req_address->email) ? $req_address->email : null;

    if (!$first_name && isset($request->consumer->first_name))
      $first_name = $request->consumer->first_name;

    if (!$last_name && isset($request->consumer->last_name))
      $last_name = $request->consumer->last_name;

    if (!$email && isset($request->consumer->email))
      $email = $request->consumer->email;

    $city  = isset($req_address->suburb) ? $req_address->suburb : null;

    if (!$city)
      $city = isset($req_address->city) ? $req_address->city : null;

    $country     = isset($req_address->country) ? $req_address->country : null;
    $zip         = isset($req_address->zip) ? $req_address->zip : null;
    $state       = isset($req_address->state) ? $req_address->state : null;

    if (!$zip)
      $zip = isset($req_address->postcode) ? $req_address->postcode : null;

    $line1 = isset($req_address->line1) ? $req_address->line1 : '';
    $line2 = isset($req_address->line2) ? $req_address->line2 : '';

    // check if there's change in address
    $address_has_changed = false;

    $quote_address = $quote->get_address($address_type);


    if ($quote_address['first_name'] != $first_name
        || $quote_address['last_name'] != $last_name
        || $quote_address['address_1'] != $line1
        || $quote_address['address_2'] != $line2
        || $quote_address['country'] != $country
        || $quote_address['postcode'] != $zip
        || $quote_address['city'] != $city
        || $quote_address['state'] != $state
    ) {
      $address_has_changed = true;
    }

    if (!$address_has_changed) {
        $this->log(sprintf('%s address has not been changed at zipMoney.', $address_type));
        return false;
    }

		$this->log(sprintf('%s address has been changed at zipMoney. Update related quote address.', $address_type));
    $this->log(sprintf('Current quote address: %s', json_encode($quote_address)));


  	$updated_address = array('first_name' => $first_name,
  							 'last_name'  => $last_name ,
  							 'email' 	  => $email,
  							 'address_1'  => $line1,
  							 'address_2'  => $line2,
  							 'postcode'   => $zip,
  							 'state'	  => $state,
  							 'city'	      => $city,
  							 'country'    => $country);

  	$quote->set_address($updated_address, $address_type);

    return true;
  }

  /**
   * Returns the express checkout url.
   *
   * @access public
   * @return string
   */
	public static function getExpressCheckoutUrl()
	{
		return get_site_url(). '/'. self::EXPRESS_CHECKOUT_ROUTE;
	}

}

/**
 * Class contains method for handling express webhooks methods.
 *
 * @class    WC_ZipMoney_ExpressWebHook
 * @version  1.0.6
 * @package  zipMoney Payments
 * @author   zipMoney Payments
 */
class WC_ZipMoney_ExpressWebHook extends ZipMoney_Abstract_Express
{

  /**
   *
   * @param  WC_ZipMoney_Payment $gateway
   */
 	public function __construct( $gateway )
  {
    parent::__construct($gateway->merchant_id,$gateway->merchant_key);

    $this->_gateway   = $gateway;

    $this->expressObj = new WC_ZipMoney_Express( $gateway );
  }

  /**
   * Retrieves the quote details and prints them as json string.
   *
   * @access protected
   * @param  array $request
   */
  protected function _actionGetQuoteDetails($request)
	{

		WC_ZipMoney::staticLog("==_actionGetQuoteDetails==\nRequest:- " . json_encode($request), $this->_gateway->debug);

		if(!isset($request->quote_id) || empty($request->quote_id))
			throw new ZipMoney_Exception("Quote Id not found in the request", 1);

		$quote = new WC_ZipMoney_Quote($request->quote_id);

		if(!$quote || !$quote->id)
			throw new ZipMoney_Exception("No quote found with the given quote id", 1);

		// Prepare Response
		$responseArray = $this->expressObj->prepareResponseForExpress( $quote, $request );

    WC_ZipMoney::staticLog("Response:- ".json_encode($responseArray), $this->_gateway->debug);

		$this->sendResponse($responseArray);
	}

  /**
   * Retrieves the quote details with shipping methods based on address provided in the request and prints them as json string.
   *
   * @access protected
   * @param  array $request
   */
	protected function _actionGetShippingMethods($request)
	{
		global $woocommerce;

		WC_ZipMoney::staticLog("==_actionGetShippingMethods==\nRequest:- " . json_encode($request), $this->_gateway->debug);


		if(!isset($request->quote_id) || empty($request->quote_id))
			throw new ZipMoney_Exception("Quote Id not found in the request", 1);

		if(!isset($request->shipping_address) || empty($request->shipping_address))
			throw new ZipMoney_Exception("Shipping Address not found in the request", 1);

		$quote = new WC_ZipMoney_Quote($request->quote_id);

		if(!$quote || !$quote->id)
			throw new ZipMoney_Exception("No quote found with the given quote id", 1);


		// Update Shipping Address if required
		$this->expressObj->updateAddress( $quote, $request  , WC_ZipMoney_Express::QUOTE_ADDRESS_TYPE_SHIPPING);
		// Update Billing Address if required
		$this->expressObj->updateAddress( $quote, $request  , WC_ZipMoney_Express::QUOTE_ADDRESS_TYPE_BILLING);

		$responseArray = $this->expressObj->prepareResponseForExpress( $quote, $request );

		$shipping_address  = $responseArray['shipping_address'] ;

    // Load shipping
    $shipping_address['options']  = $this->_getShippingMethods($quote,$shipping_address) ;


		$responseArray['shipping_address'] = $shipping_address;

    // Load shipping
    $responseArray['shipping_address']['selected_option_id']  = $request->shipping_address->selected_option_id;
    $responseArray['shipping_address']['OneLineAddress']  = $request->shipping_address->OneLineAddress;

    WC_ZipMoney::staticLog("Response:- ".json_encode( $responseArray ), $this->_gateway->debug);

		$this->sendResponse($responseArray);
	}


  /**
   * Retrieves the list of enabled shipping methods.
   *
   * @access protected
   * @param  array $options
   */
  protected function _getShippingMethods($quote, $shipping_address)
  {
    global $woocommerce;

    $woocommerce->cart->calculate_shipping();
    $packages  = $woocommerce->cart->get_shipping_packages();

    if(isset($shipping_address['state']) && !empty($shipping_address['state']))
      $packages[0]['destination']['state']  = $shipping_address['state'];

    if(isset($shipping_address['zip']) && !empty($shipping_address['zip']))
      $packages[0]['destination']['postcode']  = $shipping_address['zip'];

    if(isset($shipping_address['city']) && !empty($shipping_address['city']))
      $packages[0]['destination']['city']  = $shipping_address['city'];

    if(isset($shipping_address['line1']) && !empty($shipping_address['line1']))
      $packages[0]['destination']['address']  = $shipping_address['line1'];

    if(isset($shipping_address['line2']) && !empty($shipping_address['line2']))
      $packages[0]['destination']['address_2']  = $shipping_address['line2'];


    WC()->shipping->calculate_shipping($packages);

    $packages = WC()->shipping->packages;

    if(isset($packages[0]['rates']) && count($packages[0]['rates'])){

      $shipping_methods = $packages[0]['rates'];
      WC_ZipMoney::staticLog("Loading shipping methods from packages", $this->_gateway->debug);

    } else {
      // Load all enabled shipping methods
      $woocommerce->shipping->load_shipping_methods();
      $shipping_methods = $woocommerce->shipping->get_shipping_methods();
      WC_ZipMoney::staticLog("Loading shipping methods", $this->_gateway->debug);
    }

    if(!$shipping_methods)
      throw new ZipMoney_Exception("No shipping methods configured", 1);

    foreach ($shipping_methods as $key => $method) {

      if(isset($method->enabled) && $method->enabled == 'no') continue;

      $options[] = array('id'=>$method->id,
                         'name' => isset($method->method_title) ? $method->method_title: $method->label,
                         'value'=> isset($method->cost) && !empty($method->cost)?(int)$method->cost : 0
                        );
    }

    WC_ZipMoney::staticLog("Shipping Methods Options:- ".json_encode($options), $this->_gateway->debug);

    return $options;
  }

  /**
   * Sets the selected shipping method in the quote object passed in the request.
   *
   * @access protected
   * @param  array $request
   */
	protected function _actionConfirmShippingMethods($request)
	{
		global $woocommerce;

		WC_ZipMoney::staticLog("==_actionConfirmShippingMethods==\nRequest:- ". json_encode($request), $this->_gateway->debug);

    if(!isset($request->quote_id) || empty($request->quote_id))
			throw new ZipMoney_Exception("Quote Id not found in the request", 1);

		if(!isset($request->shipping_address) || empty($request->shipping_address))
			throw new ZipMoney_Exception("Shipping Address not found in the request", 1);

		if(!isset($request->shipping_address->options) || empty($request->shipping_address->options))
			throw new ZipMoney_Exception("Shipping Methods not  provided", 1);

		if(!isset($request->shipping_address->selected_option_id) || empty($request->shipping_address->selected_option_id))
			throw new ZipMoney_Exception("Shipping Method not selected", 1);

		$quote = new WC_ZipMoney_Quote($request->quote_id);

		if(!$quote || !$quote->id)
			throw new ZipMoney_Exception("No quote found with the given quote id", 1);

		foreach ($request->shipping_address->options as  $method) {
			if($method->id == $request->shipping_address->selected_option_id)
				$chosen_method = $method;
		}

		//Current shipping methods if any
		$quote_shipping_methods = $quote->get_shipping_methods();

		// Set the shipping method
		if(isset($chosen_method) && !$quote->has_shipping_method($chosen_method->id)){

			if($quote_shipping_methods)
				$quote->remove_order_items('shipping');

			$shipping        = new stdClass();
			$shipping->label = $chosen_method->name;
			$shipping->id    = $chosen_method->id;
			$shipping->cost  = $chosen_method->value;
			$shipping->taxes = array();
			$item_id         = $quote->add_shipping( $shipping );

			if ( ! defined( 'WOOCOMMERCE_CART' ) )
			     define( 'WOOCOMMERCE_CART', true );

			$quote->calculate_totals();
		}

		// Prepare Response
		$responseArray = $this->expressObj->prepareResponseForExpress( $quote, $request );


    $shipping_address  = $responseArray['shipping_address'] ;

    // Load shipping
    $shipping_address['options']  = $this->_getShippingMethods() ;

   // Load shipping
    $shipping_address['selected_option_id']  = $request->shipping_address->selected_option_id;

    $responseArray['shipping_address'] = $shipping_address;


    WC_ZipMoney::staticLog("Response:- ".json_encode($responseArray), $this->_gateway->debug);

		$this->sendResponse($responseArray);
	}


  /**
   * Confirms and processes the order using the quote.
   *
   * @access protected
   * @param  array $request
   */
	protected function _actionConfirmOrder($request)
	{
		WC_ZipMoney::staticLog("==_actionConfirmOrder==\nRequest:- ".json_encode($request), $this->_gateway->debug);

    if(!isset($request->quote_id) || empty($request->quote_id))
			throw new ZipMoney_Exception("Quote Id not found in the request", 1);

		$quote = new WC_ZipMoney_Quote($request->quote_id);

		if(!$quote || !$quote->id)
			throw new ZipMoney_Exception("No quote found with the given quote id", 1);

    $this->expressObj->convert_quote_to_order($quote);

    $responseArray = $this->expressObj->prepareResponseForExpress( $quote, $request , false);

    WC_ZipMoney::staticLog("Response:- ". json_encode($responseArray), $this->_gateway->debug);

		$this->sendResponse($responseArray);
	}

}

/**
 * Class is a subclass or the WC_Order.
 *
 * @class    WC_ZipMoney_Quote
 * @version  1.0.6
 * @package  zipMoney Payments
 * @author   zipMoney Payments
 */
class WC_ZipMoney_Quote extends WC_Order {}
