<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Class for widgets and banners rendering
 *
 * @class    WC_ZipMoney_Widgets
 * @version  1.1.0
 * @package  zipMoney Payments
 * @author   zipMoney Payments
 */
class WC_ZipMoney_Widgets
{

  const WIDGET_ASSET_TYPE_PRODUCT             = 'productwidget';
  const WIDGET_ASSET_TYPE_CART                = 'cartwidget';
  const WIDGET_ASSET_TYPE_TERMS_DIALOG        = 'termsdialog';
  const WIDGET_ASSET_TYPE_CHECKOUT_DIALOG     = 'checkoutdialog';
  const WIDGET_ASSET_TYPE_CART_BUTTON         = 'cartbutton';
  const WIDGET_ASSET_TYPE_PRODUCT_BUTTON      = 'productbutton';
  const WIDGET_ASSET_TYPE_CART_BUTTON_LINK    = 'cartbuttonlink';
  const WIDGET_ASSET_TYPE_PRODUCT_BUTTON_LINK = 'productbuttonlink';
  const WIDGET_ASSET_TYPE_STRIP_BANNER        = 'stripbanner';
  const WIDGET_ASSET_TYPE_SIDE_BANNER         = 'sidebanner';

  protected  $_gateway = null;

  protected  $_options    = null;
  public static $count = 0;

  /**
   *
   * @param  WC_ZipMoney_Payment $gateway
   */
  public function __construct($gateway)
  {
    $this->_options = get_option( $gateway->plugin_id . $gateway->id . '_api_settings',true);

  }

  /**
   * Returns the assets info by type.
   *
   * @access public
   * @param  string $asset_type
   * @return mixed boolean | array $assetArray
   */
  public function _get_asset($asset_type)
  {
    $assetArray = array();

    if(!isset($this->_options['assets']))
      return false;

    foreach ($this->_options['assets'] as $asset)
    {
      if(!isset($asset['type']) || $asset['type'] != $asset_type)
        continue;
      $assetArray[] = $asset;
    }

    return $assetArray;
  }

  /**
   * Returns the assets content by type. Fetches the content from the CDN if required
   *
   * @access private
   * @param  string $asset_type
   * @return array $_asset_content
   */
  private function _get_asset_content($asset_type)
  {
    $_assets   = $this->_get_asset($asset_type);

    if(count($_assets) == 0)
      return false;

    $_asset_content = array();

    if(!is_array($_assets))
      return false;

    foreach ($_assets as $key => $asset) {

      switch($asset['content_type'])
      {
        case 'html':
          if(!isset($asset['url']) || empty($asset['url']) )
            return false;

          $_url_contents    = wp_remote_get($asset['url']);
          $_asset_content[] = $_url_contents['body'];
        break;

        case 'image':

          if(!isset($asset['url']) || empty($asset['url']) )
            return false;
          $_asset_content[] = $asset['url'];
        break;

        case 'text':
          if(!isset($asset['value']) || empty($asset['value']) )
            return false;
          $_asset_content[] = $asset['value'];
        break;
      }
    }

    return $_asset_content;
  }

  /**
   * Process the html content. Replaces the token in the html with the corresponding values.
   *
   * @access private
   * @param  string $html
   * @return string $html
   */
  private function _process_html($html)
  {
    if(!$html)
      return false;

    $html = str_replace("\r", "", $html);
    $html = str_replace("\n", "", $html);

    if(!isset($this->_options['asset_values']))
      return $html;

    foreach ($this->_options['asset_values'] as $asset_value)
    {
      if(!isset($asset_value['key']) || !isset($asset_value['value']))
        continue;

      $html = str_replace($asset_value['key'], $asset_value['value'], $html);
    }

    return $html;
  }

  /**
   * Returns the banner html. Replaces the token in the html with the corresponding values.
   *
   * @access public
   * @param  string $html
   * @return string $html
   */
  public function get_banner_html($type)
  {
    $page = $this->_get_asset_content($type);

    return $this->_process_html($page[0]);
  }

  /**
   * Returns the popup assets html as an array.
   *
   * @access public
   * @param  string $type
   * @return mixed |  boolean false, array $processed_page
   */
  public function get_dialog($type)
  {
    if(!$type)
      throw new Exception("Dialog type not provided");

    $pages = $this->_get_asset_content($type);

    if(is_array($pages))
    {
      foreach ($pages as $page)
        $processed_page[] = $this->_process_html($page);

      return $processed_page;
    }

    return false;
  }

  /**
   * Returns the express checkout button url.
   *
   * @access public
   * @return string $urls
   */
  public function get_express_checkout_button_url()
  {
    $urls = $this->_get_asset_content(self::WIDGET_ASSET_TYPE_PRODUCT_BUTTON);
   return $urls[0];
  }

  /**
   * Returns the product widget url.
   *
   * @access public
   * @return string $urls
   */
  public function get_product_widget_url()
  {
    $urls = $this->_get_asset_content(self::WIDGET_ASSET_TYPE_PRODUCT);
   return $urls[0];
  }

}



/**
 * Class for widgets and banners rendering
 *
 * @class    WC_ZipMoney_Widgets_Renderer
 * @version  1.0.6
 * @package  zipMoney Payments
 * @author   zipMoney Payments
 */
class WC_ZipMoney_Widgets_Renderer
{
  private $_gateway = null;

  public $_widgetsHelper = null;

  private $_rendered = false;

  /**
   *
   * @param  WC_ZipMoney_Payment $gateway
   */
  public function __construct($gateway)
  {
    $this->_gateway = $gateway;

    $this->_widgetsHelper = new WC_ZipMoney_Widgets($gateway);
  }

  /**
   * Renders the banner in the shop page.
   *
   * @access public
   */
  public function render_banner_shop()
  {
    if(is_shop())
      $this->_render_banner();
  }

  /**
   * Renders the banner in the cart page.
   *
   * @access public
   */
  public function render_banner_cart()
  {
    if(is_cart())
     $this->_render_banner();
  }

  /**
   * Renders the banner in the product page.
   *
   * @access public
   */
  public function render_banner_productpage()
  {
    if(is_product())
      $this->_render_banner();
  }

  /**
   * Renders the banner in the category page.
   *
   * @access public
   */
  public function render_banner_category()
  {
    if(is_product_category())
      $this->_render_banner();
  }

  /**
   * Renders the element which contains the merchant public key and the api environment.
   *
   * @access public
   */
  public function render_root_el()
  {
    echo '<div data-zm-merchant="'.$this->_gateway->api_settings['merchant_public_key'].'" data-env="'.$this->_gateway->environment.'"></div> ';
  }

  public function get_checkout_redirect_url()
  {
    global $product;

    if($this->_gateway->is_iframe_flow):
      $url = get_home_url() . "/zipmoneypayment/expresscheckout/getredirecturl/" ;
    else:
      $url = WC_ZipMoney_Express::getExpressCheckoutUrl();
    endif;

    if(is_product()){
      $checkout_url = add_query_arg( array(
          'product_id'      => $product->id,
          'checkout_source' => 'product_page'
      ), $url);
    } elseif(is_cart()){
      $checkout_url = add_query_arg( array(
          'checkout_source' => 'cart'
      ), $url);
    } else {
      $checkout_url = add_query_arg( array(
          'checkout_source' => 'checkout'
      ), $url);
    }

    return $checkout_url;

  }

  /**
   * Renders the express payment button.
   *
   * @access public
   */
  public function render_express_payment_button()
  {
    $checkout_url = $this->get_checkout_redirect_url();
  ?>

  <?php
  // Is is iframe flow
    if($this->_gateway->is_iframe_flow):
  ?>
    <script type="text/javascript">
    jQuery( document ).ready(function(){
      jQuery("#checkout_express_cart").click(function(){
        var vUrl   = "<?php echo $checkout_url; ?>";
        var iframe = new iframeCheckout(vUrl, '<?php echo $class; ?>');
            iframe.redirectToCheckout();
      })
    })
    </script>
  <?php endif; ?>
    <div class="zip-express-payment-button">
      <a id="checkout_express_cart" class="zipmoney-strip-banner"  zm-widget="inline" zm-asset="productbutton"   <?php echo !$this->_gateway->is_iframe_flow?"href='".$checkout_url."'":null; ?>  style="cursor: pointer"></a>
      <a zm-widget="popup" zm-asset="<?php if(is_product()) echo 'productbuttonlink'; elseif(is_cart()) echo 'cartbuttonlink'; ?>" class="zip-productbuttonlink" zm-popup-asset="termsdialog"></a>
      <span class="please-wait zm-loader" id="redirecting-to-zipmoney" style="display: none ;" ><span class="text">Redirecting to zipMoney ...</span></span>
    </div>

      <script>
          if(window.$zmJs!==undefined) {
              //select all the elements inside div.zip-express-payment-button to recreate the widget only.
              //otherwise, it will generate multiple banner ads for each ajax request.
              jQuery('div.wc-proceed-to-checkout [zm-widget],[data-zm-widget]').each(function(index, widgetEl){
                  window.$zmJs._createWidget(widgetEl, $zmJs);
              });
          }

      </script>

  <?php
  }

   /**
   * Renders the place order button in the checkout page required for iframe/in-content checkout flow.
   *
   * @access public
   */
  public function order_button($text){
    $checkout_url = $this->get_checkout_redirect_url();
  ?>
    <script type="text/javascript">
      var is_express = <?php echo $this->_gateway->is_express? 1 : 0; ?>;

      jQuery(function(){

        jQuery("#place_order").on('click',function(e){
          console.log(jQuery('form[name="checkout"] input[name="payment_method"]:checked').val());

          if(jQuery('form[name="checkout"] input[name="payment_method"]:checked').val() == 'zipmoney'){
            if(is_express){
              var vUrl   = "<?php echo $checkout_url; ?>";
              var iframe = new iframeCheckout(vUrl, '<?php echo $class; ?>');
                  iframe.redirectToCheckout();
            } else {
              var vUrl   = wc_checkout_params.checkout_url;
              var iframe = new iframeCheckout(vUrl, 'cart', jQuery( 'form.checkout' ), '');
                  iframe.standardRedirectToCheckout();
            }
            e.preventDefault();
          }
        });

      });
    </script>
    <?php
    return $text;
  }

  /**
   * Renders the widget below add to cart / proceed to checkout button in product or cart pages.
   *
   * @access public
   */
  public function render_widget_general()
  {
    echo '<div class="widget-product-cart" data-zm-asset="productwidget" data-zm-widget="popup"  data-zm-popup-asset="termsdialog"></div>';
  }


  /**
   * Renders the widget below add to cart / proceed to checkout button in product or cart pages.
   *
   * @access public
   */
  public function render_widget_cart()
  {
    echo '<div class="widget-cart" data-zm-asset="cartwidget" zm-widget="popup"  data-zm-popup-asset="termsdialog"></div>';
  }


  /**
   * Renders the widget below add to cart / proceed to checkout button in product or cart pages.
   *
   * @access public
   */
  public function render_widget_product()
  {
    echo '<div class="widget-product" data-zm-asset="productwidget" data-zm-widget="popup"  data-zm-popup-asset="termsdialog"></div>';
  }


  /**
   * Renders the widget below add to cart / proceed to checkout button in product or cart pages.
   *
   * @access public
   */
  public function render_calc()
  {
    if(is_product()){
      global $product;

      $amount = get_post_meta($product->id , '_price', true);

    } elseif(is_cart()){
      global $woocommerce;

      $amount = WC()->cart->total;
    }

    echo '<span class="repay-calc" data-zm-widget="repaycalc" data-zm-amount="'.$amount.'" ></span> <a id="zipmoney-learn-more" class="zip-hover"  zm-widget="popup"  zm-popup-asset="termsdialog">Learn More</a>';
  }


    /**
     * Renders the widget below add to cart / proceed to checkout button in product or cart pages.
     */
  public function render_tagline()
  {
      echo '<div id="zip-tagline" data-zm-widget="tagline"  data-zm-info="true"></div>';
  }

  /**
   * Renders the banner across the shop, cart, product, category pages.
   *
   * @access private
   */
  private function _render_banner()
  {
    echo '<div class="zipmoney-strip-banner"  zm-asset="stripbanner"   zm-widget="popup"  zm-popup-asset="termsdialog" ></div>';
  }

  /**
   * Updated the method description text to include the Learn More link.
   *
   * @access public
   * @param string $description, string $id
   * @return string $description
   */
  public function updateMethodDescription($description,$id)
  {
    if($id!=$this->_gateway->id) return $description;

    if(preg_match("/Learn More/", $description)) return $description;

    $html = null;

   return $description.' <a  id="zipmoney-learn-more" class="zip-hover"  zm-widget="popup"  zm-popup-asset="termsdialog">Learn More</a>
    <script>if(window.$zmJs!==undefined) window.$zmJs._collectWidgetsEl(window.$zmJs);</script>';
  }
}
