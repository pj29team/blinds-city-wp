<br/><br/>

<?php
$dropDown = new Woo_Feed_Dropdown();
$product = new Woo_Feed_Products();
?>

<form action="" name="feed"
      method="post">

<table class=" widefat fixed">
    <tbody>
    <tr>
        <td width="30%"><b>Provider <span class="requiredIn">*</span></b></td>
        <td>
            <select name="provider" id="provider" class="generalInput">
                <?php echo $dropDown->merchantsDropdown($feedRules['provider']); ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><b>File Name <span class="requiredIn">*</span></b></td>
        <td><input name="filename"
                   value="<?php echo isset($feedRules['filename']) ? $feedRules['filename'] : ''; ?>" type="text"
                   class="generalInput"/></td>
    </tr>
    <tr>
        <td><b>Feed Type <span class="requiredIn">*</span></b></td>
        <td>
            <select name="feedType" id="feedType" class="generalInput">
                <option <?php echo ($feedRules['feedType'] == "xml") ? 'selected="selected"' : ''; ?> value="xml">
                    XML
                </option>
                <option <?php echo ($feedRules['feedType'] == "csv") ? 'selected="selected"' : ''; ?> value="csv">
                    CSV
                </option>
                <option <?php echo ($feedRules['feedType'] == "txt") ? 'selected="selected"' : ''; ?> value="txt">
                    TXT
                </option>
            </select>
        </td>
    </tr>
    <tr class="itemWrapper" <?php echo ($feedRules['feedType'] != "xml") ? 'style="display: none;"' : ''; ?> >
        <td><b>Items Wrapper <span class="requiredIn">*</span></b></td>
        <td><input name="itemsWrapper" type="text"
                   value="<?php echo ($feedRules['feedType'] == "xml") && isset($feedRules['itemsWrapper']) ? $feedRules['itemsWrapper'] : 'products'; ?>"
                   class="generalInput" required="required"/>
        </td>
    </tr>
    <tr class="itemWrapper" <?php echo ($feedRules['feedType'] != "xml") ? 'style="display: none;"' : ''; ?>>
        <td><b>Single Item Wrapper <span class="requiredIn">*</span></b></td>
        <td><input name="itemWrapper" type="text"
                   value="<?php echo ($feedRules['feedType'] == "xml") && isset($feedRules['itemWrapper']) ? $feedRules['itemWrapper'] : 'product'; ?>"
                   class="generalInput" required="required"/>
        </td>
    </tr>
    <tr class="itemWrapper" <?php echo ($feedRules['feedType'] != "xml") ? 'style="display: none;"' : ''; ?>>
        <td><b>Extra Header </b></td>
        <td>
            <textarea name="extraHeader" id="" cols="30" rows="3"><?php echo ($feedRules['feedType'] == "xml") && isset($feedRules['extraHeader']) ? $feedRules['extraHeader'] : ''; ?></textarea>
        </td>
    </tr>
    <tr class="wf_csvtxt" <?php echo ($feedRules['feedType'] == "xml") ? 'style="display: none;"' : ''; ?>>
        <td><b>Delimiter <span class="requiredIn">*</span></b></td>
        <td>
            <select name="delimiter" id="delimiter" class="generalInput">
                <option <?php echo isset($feedRules['delimiter']) && $feedRules['delimiter'] == "," ? 'selected="selected"' : ''; ?>
                    value=",">Comma
                </option>
                <option <?php echo isset($feedRules['delimiter']) && $feedRules['delimiter'] == "tab" ? 'selected="selected"' : ''; ?>
                    value="tab">Tab
                </option>
                <option <?php echo isset($feedRules['delimiter']) && $feedRules['delimiter'] == ":" ? 'selected="selected"' : ''; ?>
                    value=":">Colon
                </option>
                <option <?php echo isset($feedRules['delimiter']) && $feedRules['delimiter'] == " " ? 'selected="selected"' : ''; ?>
                    value=" ">Space
                </option>
                <option <?php echo isset($feedRules['delimiter']) && $feedRules['delimiter'] == "|" ? 'selected="selected"' : ''; ?>
                    value="|">Pipe
                </option>
                <option <?php echo isset($feedRules['delimiter']) && $feedRules['delimiter'] == ";" ? 'selected="selected"' : ''; ?>
                    value=";">Semi Colon
                </option>
            </select>
        </td>
    </tr>
    <tr class="wf_csvtxt" <?php echo ($feedRules['feedType'] == "xml") ? 'style="display: none;"' : ''; ?>>
        <td><b>Enclosure <span class="requiredIn">*</span></b></td>
        <td>
            <select name="enclosure" id="enclosure" class="generalInput">
                <option <?php echo isset($feedRules['enclosure']) && $feedRules['enclosure'] == " " ? 'selected="selected"' : ''; ?>
                    value=" ">None
                </option>
                <option <?php echo isset($feedRules['enclosure']) && $feedRules['enclosure'] == "double" ? 'selected="selected"' : ''; ?>
                    value='double'>"
                </option>
                <option <?php echo isset($feedRules['enclosure']) && $feedRules['enclosure'] == "single" ? 'selected="selected"' : ''; ?>
                    value="single">'
                </option>
            </select>
        </td>
    </tr>
    </tbody>
</table>
<br/>

<div class="container">
<section>
<div class="tabs tabs-style-bar">
    <nav>
        <ul>
            <li class="tab-current"><a href="#section-bar-1" class="icon icon-tools"><span>Content</span></a>
            </li>
            <li class=""><a href="#section-bar-2" class="icon icon-upload"><span>FTP</span></a></li>
        </ul>
    </nav>
    <div class="content-wrap">
        <section id="section-bar-1" class="content-current">
            <table class="table tree widefat fixed sorted_table mtable" width="100%" id="table-1">
                <thead>
                <tr>
                    <th></th>
                    <th>Merchant Attributes</th>
                    <th>Prefix</th>
                    <th>Type</th>
                    <th>Value</th>
                    <th>Suffix</th>
                    <th>Output Type</th>
                    <th>Output Limit</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (count($feedRules['mattributes']) > 0) {
                    $mAttributes = $feedRules['mattributes'];
                    $wooAttributes = $feedRules['attributes'];
                    $type = $feedRules['type'];
                    $default = $feedRules['default'];
                    $prefix = $feedRules['prefix'];
                    $suffix = $feedRules['suffix'];
                    $outputType = $feedRules['output_type'];
                    $limit = $feedRules['limit'];
                    //echo "<pre>";print_r($outputType);
                    $counter = 0;
                    foreach ($mAttributes as $merchant => $mAttribute) {

                        ?>
                        <tr>
                            <td>
                                <i class="wf_sortedtable dashicons dashicons-menu"></i>
                            </td>
                            <td>
                                <input type="text" name="mattributes[]" value="<?php echo $mAttribute; ?>" required
                                       class="wf_mattributes"/>
                                <!--                                        <select name="mattributes[]" id="" class="wf_mattributes">-->
                                <!--                                            --><?php //echo $dropDown->nextagAttributesDropdown($mAttribute); ?>
                                <!--                                        </select>-->
                            </td>
                            <td>
                                <input type="text" name="prefix[]" value="<?php echo $prefix[$merchant]; ?>"
                                       autocomplete="off"
                                       class="wf_ps"/>
                            </td>
                            <td>
                                <select name="type[]" id="" class="attr_type">
                                    <option <?php echo ($type[$merchant] == "attribute") ? 'selected="selected"' : ''; ?>
                                        value="attribute">Attribute
                                    </option>
                                    <option <?php echo ($type[$merchant] == "pattern") ? 'selected="selected"' : ''; ?>
                                        value="pattern">Pattern
                                    </option>
                                </select>

                            </td>
                            <td>
                                <select <?php echo ($type[$merchant] == "attribute") ? '' : 'style=" display: none;"'; ?>
                                    name="attributes[]" id=""
                                    class="wf_attr wf_attributes">
                                    <?php echo $product->attributeDropdown($wooAttributes[$merchant]); ?>
                                </select>

                                <input <?php echo ($type[$merchant] == "pattern") ? '' : 'style=" display: none;"'; ?>
                                    autocomplete="off"
                                    class="wf_default wf_attributes" type="text" name="default[]"
                                    value="<?php echo $default[$merchant]; ?>"/>

                            </td>
                            <td>
                                <input type="text" name="suffix[]" value="<?php echo $suffix[$merchant]; ?>"
                                       autocomplete="off"
                                       class="wf_ps"/>
                            </td>

                            <td>
                                <select name="output_type[<?php echo $counter; ?>][]" id=""
                                        class="outputType" <?php echo (count($outputType[$counter]) > 1) ? 'multiple="multiple"' : ''; ?>>
                                    <option <?php echo (in_array('1', $outputType[$counter])) ? 'selected="selected"' : ''; ?>
                                        value="1">Default
                                    </option>
                                    <option <?php echo (in_array('2', $outputType[$counter])) ? 'selected="selected"' : ''; ?>
                                        value="2">Strip Tags
                                    </option>
                                    <option <?php echo (in_array('3', $outputType[$counter])) ? 'selected="selected"' : ''; ?>
                                        value="3">UTF-8 Encode
                                    </option>
                                    <option <?php echo (in_array('4', $outputType[$counter])) ? 'selected="selected"' : ''; ?>
                                        value="4">htmlentities
                                    </option>
                                    <option <?php echo (in_array('5', $outputType[$counter])) ? 'selected="selected"' : ''; ?>
                                        value="5">Integer
                                    </option>
                                    <option <?php echo (in_array('6', $outputType[$counter])) ? 'selected="selected"' : ''; ?>
                                        value="6">Price
                                    </option>
                                    <option <?php echo (in_array('7', $outputType[$counter])) ? 'selected="selected"' : ''; ?>
                                        value="7">Remove Space
                                    </option>
                                    <option <?php echo (in_array('8', $outputType[$counter])) ? 'selected="selected"' : ''; ?>
                                        value="8">CDATA
                                    </option>
                                </select>
                                <i class="dashicons dashicons-editor-expand expandType"></i>
                                <i style="display: none;"
                                   class="dashicons dashicons-editor-contract contractType"></i>
                            </td>
                            <td>
                                <input type="text" name="limit[]" value="<?php echo $limit[$merchant]; ?>"
                                       autocomplete="off"
                                       class="wf_ps"/>
                            </td>
                            <td>
                                <i class="delRow dashicons dashicons-trash"></i>
                            </td>
                        </tr>

                        <?php
                        $counter++;
                    }
                }
                ?>

                </tbody>
                <tfoot>
                <tr>
                    <td colspan="3">
                        <button type="button" class="button-small button-primary" id="wf_newRow">Add New Row
                        </button>
                    </td>
                    <td colspan="6">

                    </td>
                </tr>
                </tfoot>
            </table>
        </section>
        <section id="section-bar-2" class="">
            <table class="table widefat fixed mtable" width="100%">
                <tbody>
                <tr>
                    <td>Enabled</td>
                    <td>
                        <select name="enabled" id="">
                            <option <?php echo ($feedRules['passive'] == "0") ? 'selected="selected"' : ''; ?>
                                value="0">Disabled
                            </option>
                            <option <?php echo ($feedRules['passive'] == "1") ? 'selected="selected"' : ''; ?>
                                value="1">Enabled
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Protocol</td>
                    <td>
                        <select name="protocol" id="">
                            <option <?php echo ($feedRules['enabled'] == "ftp") ? 'selected="selected"' : ''; ?>
                                value="ftp">FTP/FTPS
                            </option>
                            <option <?php echo ($feedRules['enabled'] == "sftp") ? 'selected="selected"' : ''; ?>
                                value="sftp">SFTP
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Host Name</td>
                    <td><input type="text" value="<?php echo $feedRules['ftphost']; ?>" name="ftphost"
                               autocomplete="off"/></td>
                </tr>
                <tr>
                    <td>User Name</td>
                    <td><input type="text" value="<?php echo $feedRules['ftpuser']; ?>" name="ftpuser"
                               autocomplete="off"/></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="password" value="<?php echo $feedRules['ftppassword']; ?>"
                               name="ftppassword" autocomplete="off"/></td>
                </tr>
                <tr>
                    <td>Path</td>
                    <td><input type="text" value="<?php echo $feedRules['ftppath']; ?>" name="ftppath"
                               autocomplete="off"/></td>
                </tr>
                <tr>
                    <td>Passive Mode</td>
                    <td>
                        <select name="passive" id="">
                            <option <?php echo ($feedRules['passive'] == "0") ? 'selected="selected"' : ''; ?>
                                value="0">Disabled
                            </option>
                            <option <?php echo ($feedRules['passive'] == "1") ? 'selected="selected"' : ''; ?>
                                value="1">Enabled
                            </option>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
        </section>
    </div>
    <!-- /content -->
</div>
</section>
</div>
<!-- /container -->
<table class=" widefat fixed">
    <tr>
        <td align="right">
            <button name="<?php echo isset($_GET['action']) ? $_GET['action'] : ''; ?>"
                    type="submit" id="submit"
                    class="wfbtn">
                Update and Generate Feed
            </button>
        </td>
    </tr>
</table>
</form>
<script>
    // Tab Initialize
    [].slice.call(document.querySelectorAll('.tabs')).forEach(function (el) {
        new CBPFWTabs(el);
    });
</script>
