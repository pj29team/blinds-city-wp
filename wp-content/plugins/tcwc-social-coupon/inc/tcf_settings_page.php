<?PHP

/*-----------------------------------------------------------------------------------*/
/*	Menu Creation
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_create_menu() {
	
	$page = add_submenu_page( 'woocommerce', __("Social Coupon for WooCommerce", "wc_tcsd"), __("Social Coupon", "wc_tcsd"), 'manage_options', WC_TCSD_PATH.'/inc/tcf_settings.php', 'wc_tcsd_settings_page' );

	// call register settings function
	add_action( 'admin_init', 'wc_tcsd_register_settings' );
	
	// Hook style sheet loading
	add_action( 'admin_print_styles-' . $page, 'wc_tcsd_admin_cssloader' );

}

/*-----------------------------------------------------------------------------------*/
/*	Add Admin CSS
/*-----------------------------------------------------------------------------------*/

// Add style sheet for plugin settings
function wc_tcsd_settings_admin_css(){
				
	/* Register our stylesheet. */
	wp_register_style( 'wc_tcsdSettings', WC_TCSD_LOCATION.'/css/tc_framework.css' );
							
} function wc_tcsd_admin_cssloader(){
	
	// It will be called only on your plugin admin page, enqueue our stylesheet here
	wp_enqueue_style( 'wc_tcsdSettings' );
	   
} // End admin style CSS

/*-----------------------------------------------------------------------------------*/
/*	Define Settings
/*-----------------------------------------------------------------------------------*/

global $wc_tcsc_settings;

$first_time_string = str_replace('.', '', WC_TCSD_VERSION).'-'.rand(1000, 999999999);

$wc_tcsc_settings = array(	
	// Basic Setup Options
	'enabled'				=> 'false',
	'checkout'				=> 'true',
	'checkout-position'		=> 'woocommerce_checkout_before_customer_details',
	'cart'					=> 'true',
	'cart-position'			=> 'woocommerce_cart_collaterals',
	'product'				=> 'true',
	'product-position'		=> 'woocommerce_after_single_product_summary',
	'discount-code'			=> '',
	'discount-type'			=> 'single',
	'expires'				=> '24',
	'title'					=> 'Save On Your Purchase by Sharing!',
	'message'				=> 'Simply give us a share and a special discount will be applied to your purchase at checkout!',
	'button-covers'			=> 'true',
	'https-enabled'			=> 'false',
	// Facebook Like Setup
	'facebook-enabled'		=> 'true',
	'facebook-colorscheme'  => 'light',
	'facebook-url'			=> 'CURRENT',
	'facebook-url-products'	=> 'CURRENT',
	'facebook-api-locale'	=> 'en_US',
	'facebook-like-code'	=> '',
	// Facebook Share Setup
	'facebook-share-enabled' => 'false',
	'facebook-share-url'	=> 'CURRENT',
	'facebook-share-url-products'  => 'CURRENT',
	'facebook-share-label'  => 'Share',
	'facebook-share-code'	=> '',
	'facebook-app-id'  		=> '',
	'facebook-share-title'	=> 'Check out Social Coupon for WooCommerce!',
	'facebook-share-desc'	=> 'Social Coupon for WordPress allows you to give users discounts on WooCommerce for sharing your product pages, following you on Twitter, and more!',
	'facebook-share-image'	=> 'http://0.s3.envato.com/files/41005034/tcwc-icon.png',
	// Google+ Setup
	'google-enabled'		=> 'true',
	'google-url'			=> 'CURRENT',
	'google-url-products'	=> 'CURRENT',
	'google-api-locale'		=> 'en-US',
	'google-plus-code'		=> '',
	// Twitter Follow / Tweet Setup
	'twitter-enabled'		=> 'true',
	'twitter-tweet-enabled'	=> 'true',
	'twitter-username'		=> 'TylerColwell7',
	'twitter-counter'		=> 'CURRENT',
	'twitter-url'			=> 'CURRENT',
	'twitter-url-products'  => 'CURRENT',
	'twitter-tweet-text'	=> '',
	'twitter-api-locale'	=> 'en',
	'twitter-tweet-code'	=> '',
	'twitter-follow-code'	=> '',
	// LinkedIn Setup
	'linkedin-enabled'		=> 'true',
	'linkedin-url'			=> 'CURRENT',
	'linkedin-url-products'	=> 'CURRENT',
	'linkedin-api-locale'	=> 'en_US',
	'linkedin-code'			=> '',
	// VK Setup
	'vk-enabled'			=> 'true',
	'vk-url'				=> 'CURRENT',
	'vk-url-products'		=> 'CURRENT',
	'vk-api-locale'			=> 'en_US',
	'vk-app-id'				=> '',
	'vk-code'				=> '',
	// PRIVATE USAGE
	'discount-string'		=> $first_time_string
	
);

/*-----------------------------------------------------------------------------------*/
/*	Register Settings
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_register_settings(){
	global $wc_tcsc_settings;
	$prefix = 'wc-tcsd';
	foreach($wc_tcsc_settings as $setting => $value){
		// Define
		$thisSetting = $prefix.'-'.$setting;
		// Register setting
		register_setting( $prefix.'-settings-group', $thisSetting );
		// Apply default
		add_option( $thisSetting, $value );
	}
}

/*-----------------------------------------------------------------------------------*/
/*	Get Settings
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_get_settings(){
	// Get Settings	
	global $wc_tcsc_settings;
	$prefix = 'wc-tcsd';
	$new_settings = array();
	foreach($wc_tcsc_settings as $setting => $default){
		// Define
		$thisSetting = $prefix.'-'.$setting;
		$value = get_option( $thisSetting );
		if( !isset($value) ) : $value = ''; endif;
		$new_settings[$setting] = $value;
	}
	return $new_settings;
}

global $wc_tcsc_options;
$wc_tcsc_options = wc_tcsd_get_settings();

/*-----------------------------------------------------------------------------------*/
/*	Global HTTPS Check
/*-----------------------------------------------------------------------------------*/

global $wc_tcsc_https;
if( get_option('wc-tcsd-https-enabled') == 'true' ){
	$wc_tcsc_https = 'https:';
} else {
	$wc_tcsc_https = 'http:';
}

/*-----------------------------------------------------------------------------------*/
/*	Coupon Menu Builder
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_coupon_menu($label, $settings_slug){
	
	// Globals
	global $wpdb;
	
	// Create List of Coupons for Menus
	$discount_codes = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type = 'shop_coupon' ORDER BY id DESC");
	$count = count($discount_codes);
	
	// If Coupons Returned
	if( $count > 0 ){
		
		echo'
		<label>'.$label.'</label>
		<select class="textfield" name="'.$settings_slug.'" type="text" id="'.$settings_slug.'">';
		foreach( $discount_codes as $discount ){
			$selected = '';
			if(get_option($settings_slug) == $discount->post_title){$selected = "selected=selected";}
			echo '<option value="'.$discount->post_title.'" '.$selected.'>'.$discount->post_title.'</option>';
			
		}
		if( get_option($settings_slug) == '' ){
			echo '<option value="" selected="selected"> </option>';
		}
		echo '</select>';
		
	} else { // No Coupons Found
		
		echo '<p>'._e('You have no WooCommerce Coupon Codes! Create some!').'</p>';
		
	} // end if count
	
}

/*-----------------------------------------------------------------------------------*/
/*	Ajax save callback
/*-----------------------------------------------------------------------------------*/

add_action('wp_ajax_wc_tcsd_tc_settings_save', 'wc_tcsd_tc_settings_save');

function wc_tcsd_tc_settings_save(){

	// Check Secure
	check_ajax_referer('wc_tcsd_settings_secure', 'security');
	
	// Setup
	global $wc_tcsc_settings;
	$prefix = 'wc-tcsd';

	// Loop through settings
	foreach($wc_tcsc_settings as $setting => $value){
		
		// Define
		$thisSetting = $prefix.'-'.$setting;
					
		// Register setting
		if( isset( $_POST[$thisSetting] ) ){
			update_option( $thisSetting, $_POST[$thisSetting] );
		}
		
	} // end for each
		
}

/*-----------------------------------------------------------------------------------*/
/*	New framework settings page
/*-----------------------------------------------------------------------------------*/

function wc_tcsd_settings_page(){
	
	// Globals
	global $wpdb;
	
?>

<script>
	
jQuery(document).ready(function(){

/*-----------------------------------------------------------------------------------*/
/*	Options Pages and Tabs
/*-----------------------------------------------------------------------------------*/
	  
	jQuery('.options_pages li').click(function(){
		
		var tab_page = 'div#' + jQuery(this).attr('id');
		var old_page = 'div#' + jQuery('.options_pages li.active').attr('id');
		
		// Change button class
		jQuery('.options_pages li.active').removeClass('active');
		jQuery(this).addClass('active');
				
		// Set active tab page
		jQuery(old_page).fadeOut('slow', function(){
			
			jQuery(tab_page).fadeIn('slow');
			
		});
		
	});
	
/*-----------------------------------------------------------------------------------*/
/*	Form Submit
/*-----------------------------------------------------------------------------------*/
	
	jQuery('form#plugin-options').submit(function(){
			
		var data = jQuery(this).serialize();
		
		jQuery.post(ajaxurl, data, function(response){
			
			if(response == 0){
				
				// Flash success message and shadow
				var success = jQuery('#success-save');
				var bg = jQuery("#message-bg");
				success.css("position","fixed");
				success.css("top", ((jQuery(window).height() - 65) / 2) + jQuery(window).scrollTop() + "px");
				success.css("left", ((jQuery(window).width() - 257) / 2) + jQuery(window).scrollLeft() + "px");
				bg.css({"height": jQuery(window).height()});
				bg.css({"opacity": .45});
				bg.fadeIn("slow", function(){
					success.fadeIn('slow', function(){
						success.delay(1500).fadeOut('fast', function(){
							bg.fadeOut('fast');
						});
					});
				});
								
			} else {
				
				//error out
				
			}
		
		});
				  
		return false;
	
	});	
	
/*-----------------------------------------------------------------------------------*/
/*	Popup Center Handles
/*-----------------------------------------------------------------------------------*/
	
	// Center Function
	jQuery.fn.center = function(parent){
		this.animate({"top":( jQuery(window).height() - this.height() - 65 ) / 2+jQuery(window).scrollTop() + "px"},100);
		//this.css({"left": (((jQuery(this).parent().width() - this.outerWidth()) / 2) + jQuery(this).parent().scrollLeft() + "px")});
		this.css({"left":"250px"});
		return this;
	}
	
	// Center onLoad and Scroll
	jQuery('#success-save').center();
	jQuery(window).scroll(function(){ 
		jQuery('#success-save').center();
	});
	
/*-----------------------------------------------------------------------------------*/
/*	Finished
/*-----------------------------------------------------------------------------------*/
	
});

</script>

<div class="wrap">

    <div id="icon-options-general" class="icon32"><br/></div>
    <h2 class="tc-heading"><?PHP _e('WooCommerce Social Coupon', 'wc_tcsd') ?> <span id="version">V<?PHP echo WC_TCSD_VERSION; ?></span> <a href="<?PHP echo WC_TCSD_LOCATION; ?>/documentation" target="_blank">&raquo; <?PHP _e('View Plugin Documentation', 'wc_tcsd') ?></a></h2>

</div>

<div id="message-bg"></div>
<div id="success-save"></div>

<div id="tc_framework_wrap">

    <div id="content_wrap">
    
    	<form id="plugin-options" name="plugin-options" action="/">
        <?php settings_fields( 'wc-tcsd-settings-group' ); ?>
        <input type="hidden" name="action" value="wc_tcsd_tc_settings_save" />
        <input type="hidden" name="security" value="<?php echo wp_create_nonce('wc_tcsd_settings_secure'); ?>" />
        <!-- Checkbox Fall Backs -->
        <input type="hidden" name="wc-tcsd-facebook-enabled" id="wc-tcsd-facebook-enabled" value="false" />
        <input type="hidden" name="wc-tcsd-facebook-share-enabled" id="wc-tcsd-facebook-share-enabled" value="false" />
        <input type="hidden" name="wc-tcsd-twitter-enabled" id="wc-tcsd-twitter-enabled" value="false" />
        <input type="hidden" name="wc-tcsd-twitter-tweet-enabled" id="wc-tcsd-twitter-tweet-enabled" value="false" />
        <input type="hidden" name="wc-tcsd-google-enabled" id="wc-tcsd-google-enabled" value="false" />
        <input type="hidden" name="wc-tcsd-linkedin-enabled" id="wc-tcsd-linkedin-enabled" value="false" />

        	<div id="sub_header" class="info">
            
                <input type="submit" name="settingsBtn" id="settingsBtn" class="button-framework save-options" value="<?php _e('Save All Changes', 'wc_tcsd') ?>" />
                <span><?PHP _e('Options Page', 'wc_tcsd') ?></span>
                
            </div>
            
            <div id="content">
            
            	<div id="options_content">
                
                	<ul class="options_pages">
                    	<li id="layout_options" class="active"><a href="#"><?php _e('General Settings', 'wc_tcsd') ?></a><span></span></li>
                    	<li id="facebook_options"><a href="#"><?php _e('Facebook Settings', 'wc_tcsd') ?></a><span></span></li>
                    	<li id="google_options"><a href="#"><?php _e('Google Settings', 'wc_tcsd') ?></a><span></span></li>
                    	<li id="twitter_options"><a href="#"><?php _e('Twitter Settings', 'wc_tcsd') ?></a><span></span></li>
                    	<li id="linkedin_options"><a href="#"><?php _e('LinkedIn Settings', 'wc_tcsd') ?></a><span></span></li>
                    	<li id="vk_options"><a href="#"><?php _e('Vkontakte Settings', 'wc_tcsd') ?></a><span></span></li>
                    </ul>
                    
                    <div id="layout_options" class="options_page"> 

                    	<div class="option">
                        	<h3><?php _e('Social Coupon Enabled', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element"><select name="wc-tcsd-enabled" id="wc-tcsd-enabled" class="textfield">
                    <option value="true" <?PHP if(get_option('wc-tcsd-enabled') == 'true'){echo 'selected="selected"';} ?>><?php _e('True', 'wc_tcsd') ?></option>
                    <option value="false" <?PHP if(get_option('wc-tcsd-enabled') == 'false'){echo 'selected="selected"';} ?>><?php _e('False', 'wc_tcsd') ?></option>
                				</select></div>
                                <div class="description"><?php _e('From here you can enable and disable the plugin', 'wc_tcsd') ?>.</div>
                            </div>
                        </div>  
                        
                    	<div class="option">
                        	<h3><?php _e('HTTPS Enabled', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element"><select name="wc-tcsd-https-enabled" id="wc-tcsd-https-enabled" class="textfield">
                    <option value="true" <?PHP if(get_option('wc--tcsd-https-enabled') == 'true'){echo 'selected="selected"';} ?>><?php _e('True', 'wc_tcsd') ?></option>
                    <option value="false" <?PHP if(get_option('wc-tcsd-https-enabled') == 'false'){echo 'selected="selected"';} ?>><?php _e('False', 'wc_tcsd') ?></option>
                				</select></div>
                                <div class="description"><?php _e('Here you can enabled HTTPS for assests loading by the plugin. This will force load all social SDKs and plugin assets using HTTPS.', 'wc_tcsd') ?></div>
                            </div>
                        </div>  
                        
                    	<div class="option">
                        	<h3><?php _e('Enable Button Covers', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element"><select name="wc-tcsd-button-covers" id="wc-tcsd-button-covers" class="textfield">
                    <option value="true" <?PHP if(get_option('wc-tcsd-button-covers') == 'true'){echo 'selected="selected"';} ?>><?php _e('True', 'wc_tcsd') ?></option>
                    <option value="false" <?PHP if(get_option('wc-tcsd-button-covers') == 'false'){echo 'selected="selected"';} ?>><?php _e('False', 'wc_tcsd') ?></option>
                				</select></div>
                                <div class="description"><?php _e('Here you can disable or enable the animated button covers used in Social Coupon discount areas.', 'wc_tcsd') ?></div>
                            </div>
                        </div>  
                                                
                    	<div class="option">
                        	<h3><?php _e('Checkout Page Setup', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                	<label><?PHP _e('Show on Checkout Page', 'wc_tcsd'); ?></label>
                                    <select name="wc-tcsd-checkout" id="wc-tcsd-checkout" class="textfield">
                                        <option value="true" <?PHP if(get_option('wc-tcsd-checkout') == 'true'){echo 'selected="selected"';} ?>><?php _e('True', 'wc_tcsd') ?></option>
                                        <option value="false" <?PHP if(get_option('wc-tcsd-checkout') == 'false'){echo 'selected="selected"';} ?>><?php _e('False', 'wc_tcsd') ?></option>
                                    </select>
									<br />
                                	<label><?PHP _e('Choose Position', 'wc_tcsd'); ?></label>
                                    <select name="wc-tcsd-checkout-position" id="wc-tcsd-checkout-position" class="textfield">
                                        <option value="woocommerce_before_checkout_form" <?PHP if(get_option('wc-tcsd-checkout-position') == 'woocommerce_before_checkout_form'){echo 'selected="selected"';} ?>><?php _e('Before Checkout Form', 'wc_tcsd') ?></option>
                                        <option value="woocommerce_checkout_after_customer_details" <?PHP if(get_option('wc-tcsd-checkout-position') == 'woocommerce_checkout_after_customer_details'){echo 'selected="selected"';} ?>><?php _e('After Customer Details', 'wc_tcsd') ?></option>
                                        <option value="woocommerce_after_checkout_form" <?PHP if(get_option('wc-tcsd-checkout-position') == 'woocommerce_after_checkout_form'){echo 'selected="selected"';} ?>><?php _e('After Checkout Form', 'wc_tcsd') ?></option>
                                    </select>                                                                        
                                </div>
                                <div class="description"><?php _e('Choose to show Social Coupon on the checkout page. You can also select from various WooCommerce template hooks to position Social Coupon.', 'wc_tcsd') ?>.</div>
                            </div>
                        </div>  
                        
                    	<div class="option">
                        	<h3><?php _e('Cart Page Setup', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                	<label><?PHP _e('Show on Cart Page', 'wc_tcsd'); ?></label>
                                    <select name="wc-tcsd-cart" id="wc-tcsd-cart" class="textfield">
                                        <option value="true" <?PHP if(get_option('wc-tcsd-cart') == 'true'){echo 'selected="selected"';} ?>><?php _e('True', 'wc_tcsd') ?></option>
                                        <option value="false" <?PHP if(get_option('wc-tcsd-cart') == 'false'){echo 'selected="selected"';} ?>><?php _e('False', 'wc_tcsd') ?></option>
                                    </select>
									<br />
                                	<label><?PHP _e('Choose Position', 'wc_tcsd'); ?></label>
                                    <select name="wc-tcsd-cart-position" id="wc-tcsd-cart-position" class="textfield">
                                        <option value="woocommerce_cart_collaterals" <?PHP if(get_option('wc-tcsd-cart-position') == 'woocommerce_cart_collaterals'){echo 'selected="selected"';} ?>><?php _e('Cart Collaterals', 'wc_tcsd') ?></option>
                                        <option value="woocommerce_before_cart_table" <?PHP if(get_option('wc-tcsd-cart-position') == 'woocommerce_before_cart_table'){echo 'selected="selected"';} ?>><?php _e('Before Cart Table', 'wc_tcsd') ?></option>
                                        <option value="woocommerce_before_cart_contents" <?PHP if(get_option('wc-tcsd-cart-position') == 'woocommerce_before_cart_contents'){echo 'selected="selected"';} ?>><?php _e('Before Cart Contents', 'wc_tcsd') ?></option>
                                        <option value="woocommerce_after_cart_contents" <?PHP if(get_option('wc-tcsd-cart-position') == 'woocommerce_after_cart_contents'){echo 'selected="selected"';} ?>><?php _e('After Cart Contents', 'wc_tcsd') ?></option>
                                        <option value="woocommerce_after_cart" <?PHP if(get_option('wc-tcsd-cart-position') == 'woocommerce_after_cart'){echo 'selected="selected"';} ?>><?php _e('After Cart', 'wc_tcsd') ?></option>
                                    </select>                                    
                                </div>
                                <div class="description"><?php _e('Choose to show Social Coupon on the cart page. You can also select from various WooCommerce template hooks to position Social Coupon.', 'wc_tcsd') ?>.</div>
                            </div>
                        </div>  
                        
                    	<div class="option">
                        	<h3><?php _e('Product Page Setup', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                	<label><?PHP _e('Show on Product Pages', 'wc_tcsd'); ?></label>
                                    <select name="wc-tcsd-product" id="wc-tcsd-product" class="textfield">
                                        <option value="true" <?PHP if(get_option('wc-tcsd-product') == 'true'){echo 'selected="selected"';} ?>><?php _e('True', 'wc_tcsd') ?></option>
                                        <option value="false" <?PHP if(get_option('wc-tcsd-product') == 'false'){echo 'selected="selected"';} ?>><?php _e('False', 'wc_tcsd') ?></option>
                                    </select>
									<br />
                                	<label><?PHP _e('Choose Position', 'wc_tcsd'); ?></label>
                                    <select name="wc-tcsd-product-position" id="wc-tcsd-product-position" class="textfield">
                                        <option value="woocommerce_before_single_product_summary" <?PHP if(get_option('wc-tcsd-product-position') == 'woocommerce_before_single_product_summary'){echo 'selected="selected"';} ?>><?php _e('Before Product Summary', 'wc_tcsd') ?></option>
                                        <option value="woocommerce_after_single_product_summary" <?PHP if(get_option('wc-tcsd-product-position') == 'woocommerce_after_single_product_summary'){echo 'selected="selected"';} ?>><?php _e('After Product Summary', 'wc_tcsd') ?></option>
                                    </select>
                                </div>
                                <div class="description"><?php _e('Choose to show Social Coupon on product pages. You can also select from various WooCommerce template hooks to position Social Coupon.', 'wc_tcsd') ?>.</div>
                            </div>
                        </div>  
                        
                    	<div class="option">
                        	<h3><?php _e('Coupon Setup', 'edd_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                	<label><?PHP _e('Discount Type', 'wc_tcsd'); ?></label>
                                    <select name="wc-tcsd-discount-type" id="wc-tcsd-discount-type" class="textfield">
                                        <option value="single" <?PHP if(get_option('wc-tcsd-discount-type') == 'single'){echo 'selected="selected"';} ?>><?php _e('Single Discount', 'wc_tcsd') ?></option>
                                        <option value="multi" <?PHP if(get_option('wc-tcsd-discount-type') == 'multi'){echo 'selected="selected"';} ?>><?php _e('Multi Discount', 'wc_tcsd') ?></option>
                                    </select>
									<br />
                                    <?PHP wc_tcsd_coupon_menu( __('Coupon Code', 'wc_tcsd'), 'wc-tcsd-discount-code'); ?>
                                </div>
                                <div class="description"><?php _e('Select the WooCommerce coupon you want to apply to the shopping cart when Social Coupon is activated. When in Single Discount mode, the user will get the coupon selected applied to their cart one time. The Multi Discount option allows the buyer to use multiple buttons each giving their own discount. Make sure to read the documentation on Multi Discount before using the feature.', 'edd_tcsd') ?>.</div>
                            </div>
                        </div>      
                                                                        
                    	<div class="option">
                        	<h3><?php _e('Usage Expiration', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element"><input class="textfield" name="wc-tcsd-expires" type="text" id="wc-tcsd-expires" value="<?php echo get_option('wc-tcsd-expires'); ?>" /></div>
                                <div class="description"><?php _e('Enter the number of hours a user has to wait before being able to use Social Coupon again. Note: Usage Expiration will not be applied when using Multi Discount.', 'wc_tcsd') ?></div>
                            </div>
                        </div>      

                    	<div class="option">
                        	<h3><?php _e('Social Coupon Title', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element"><input class="textfield" name="wc-tcsd-title" type="text" id="wc-tcsd-title" value="<?php echo get_option('wc-tcsd-title'); ?>" /></div>
                                <div class="description"><?php _e('Enter the title that appears in your Social Coupon button area', 'wc_tcsd') ?>.</div>
                            </div>
                        </div>      

                    	<div class="option">
                        	<h3><?php _e('Social Coupon Message', 'tcsocialhover') ?></h3>
                            <div class="section">
                            	<div class="element"><textarea class="textfield" name="wc-tcsd-message" cols="" rows="5" id="wc-tcsd-message"><?php echo str_replace('\\', "", get_option('wc-tcsd-message')); ?></textarea></div>
                                <div class="description"><?php _e('Enter the message that will appear inside the Social Coupon button area', 'tcsocialhover') ?>.</div>
                            </div>
                        </div>
                                                                                                
                    </div>   

                    <div id="facebook_options" class="options_page hide">
                    
                    
                        <div class="option">
                            <h3><?php _e('Facebook Color Scheme', 'wc_tcsd') ?></h3>
                            <div class="section">
                                <div class="element"><select name="wc-tcsd-facebook-colorscheme" id="wc-tcsd-facebook-colorscheme" class="textfield">
                    <option value="light" <?PHP if(get_option('wc-tcsd-facebook-colorscheme') == 'light'){echo 'selected="selected"';} ?>><?php _e('Light', 'wc_tcsd') ?></option>
                    <option value="dark" <?PHP if(get_option('wc-tcsd-facebook-colorscheme') == 'dark'){echo 'selected="selected"';} ?>><?php _e('Dark', 'wc_tcsd') ?></option>
                                </select></div>
                                <div class="description"><?php _e('Choose which color scheme to use with the Like Button', 'wc_tcsd') ?>.</div>
                            </div>
                        </div>  


                        <div class="option">
                            <h3><?php _e('Facebook API Locale', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                    <input class="textfield" name="wc-tcsd-facebook-api-locale" type="text" id="wc-tcsd-facebook-api-locale" value="<?php echo get_option('wc-tcsd-facebook-api-locale'); ?>" />
                            	</div>
                                <div class="description"><?php _e('Here you can enter a custom locale / language code to use with the Facebook JS API used for the Like and Share buttons. Note: Facebook uses ISO country codes. This will default to en_US if left blank', 'wc_tcsd') ?></div>
                            </div>
                        </div>


                        <div class="option">
                            <h3><?php _e('Like Button Setup', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                	<div class="tc-checkbox-wrap-wide">
                                    <label><input name="wc-tcsd-facebook-enabled" type="checkbox" class="tc-checkbox" id="wc-tcsd-facebook-enabled" value="true" <?PHP if( get_option('wc-tcsd-facebook-enabled') == 'true' ) : ?>checked="checked"<?PHP endif; ?> /><?PHP _e('Enable Like Button', 'wc_tcsd'); ?></label><br />
                                    </div>
                                    <br />
									<label><?PHP _e('Like Button URL', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-facebook-url" type="text" id="wc-tcsd-facebook-url" value="<?php echo get_option('wc-tcsd-facebook-url'); ?>" />
                                    <br />
									<label><?PHP _e('Like Button URL (Product Pages)', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-facebook-url-products" type="text" id="wc-tcsd-facebook-url-products" value="<?php echo get_option('wc-tcsd-facebook-url-products'); ?>" />
                                    <br />
									<?PHP wc_tcsd_coupon_menu( __('Multi Discount Code', 'wc_tcsd'), 'wc-tcsd-facebook-like-code'); ?>
                            	</div>
                                <div class="description"><?php _e('Enable and setup a like button, this can be ANY URL including FB page URLs. Note you can set different URLs for product pages and checkout / cart pages. You can also select a coupon to use with the Like Button in Multi Discount Mode.', 'wc_tcsd') ?></div>
                            </div>
                        </div>


                        <div class="option">
                            <h3><?php _e('Share Button Setup', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                	<div class="tc-checkbox-wrap-wide">
                                    <label><input name="wc-tcsd-facebook-share-enabled" type="checkbox" class="tc-checkbox" id="wc-tcsd-facebook-share-enabled" value="true" <?PHP if( get_option('wc-tcsd-facebook-share-enabled') == 'true' ) : ?>checked="checked"<?PHP endif; ?> /><?PHP _e('Enable Share Button', 'wc_tcsd'); ?></label><br />
                                    </div>
                                    <br />
									<label><?PHP _e('Facebook App ID', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-facebook-app-id" type="text" id="wc-tcsd-facebook-app-id" value="<?php echo get_option('wc-tcsd-facebook-app-id'); ?>" />
                                    <br />
									<label><?PHP _e('Share Button Label', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-facebook-share-label" type="text" id="wc-tcsd-facebook-share-label" value="<?php echo get_option('wc-tcsd-facebook-share-label'); ?>" />
                                    <br />
									<label><?PHP _e('Share Button URL', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-facebook-share-url" type="text" id="wc-tcsd-facebook-share-url" value="<?php echo get_option('wc-tcsd-facebook-share-url'); ?>" />
                                    <br />
									<label><?PHP _e('Share Button URL (Product Pages)', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-facebook-share-url-products" type="text" id="wc-tcsd-facebook-share-url-products" value="<?php echo get_option('wc-tcsd-facebook-share-url-products'); ?>" />
                                    <br />
									<label><?PHP _e('Share Title', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-facebook-share-title" type="text" id="wc-tcsd-facebook-share-title" value="<?php echo get_option('wc-tcsd-facebook-share-title'); ?>" />
                                    <br />
									<label><?PHP _e('Share Image (URL To Image File)', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-facebook-share-image" type="text" id="wc-tcsd-facebook-share-image" value="<?php echo get_option('wc-tcsd-facebook-share-image'); ?>" />
                                    <br />
									<label><?PHP _e('Share Message', 'wc_tcsd'); ?></label>
                                    <textarea class="textfield" name="wc-tcsd-facebook-share-desc" cols="" rows="5" id="wc-tcsd-facebook-share-desc"><?php echo str_replace('\\', "", get_option('wc-tcsd-facebook-share-desc')); ?></textarea>
                                    <br />
                                    <?PHP wc_tcsd_coupon_menu( __('Multi Discount Code', 'wc_tcsd'), 'wc-tcsd-facebook-share-code'); ?>
                            	</div>
                                <div class="description"><?php _e('Enable and setup a Share button, this can use ANY URL including FB page URLs. Note you can set different URLs for product pages and checkout / cart pages. You can also configure the title, message, and image used in the Share button snippet.', 'wc_tcsd') ?></div>
                            </div>
                        </div>
                        
                    </div>
                    
                    
                    <div id="google_options" class="options_page hide">            
                        
                    	<div class="option">
                        	<h3><?php _e('Google Plus API Locale', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element"><input class="textfield" name="wc-tcsd-google-api-locale" type="text" id="wc-tcsd-google-api-locale" value="<?php echo get_option('wc-tcsd-google-api-locale'); ?>" /></div>
                                <div class="description"><?php _e('Here you can enter a custom locale code to use with the Google+ API and the Plus / Share button.', 'wc_tcsd') ?> <a href="https://developers.google.com/+/web/api/supported-languages" target="_blank"><?PHP _e('Supported Languages', 'wc_tcsd'); ?></a></div>
                            </div>
                        </div>   
                        
                        <div class="option">
                            <h3><?php _e('Plus Button Setup', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                	<div class="tc-checkbox-wrap-wide">
                                    <label><input name="wc-tcsd-google-enabled" type="checkbox" class="tc-checkbox" id="wc-tcsd-google-enabled" value="true" <?PHP if( get_option('wc-tcsd-google-enabled') == 'true' ) : ?>checked="checked"<?PHP endif; ?> /><?PHP _e('Enable Plus Button', 'wc_tcsd'); ?></label><br />
                                    </div>
                                    <br />
									<label><?PHP _e('Plus Button URL', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-google-url" type="text" id="wc-tcsd-google-url" value="<?php echo get_option('wc-tcsd-google-url'); ?>" />
                                    <br />
									<label><?PHP _e('Plus Button URL (Product Pages)', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-google-url-products" type="text" id="wc-tcsd-google-url-products" value="<?php echo get_option('wc-tcsd-google-url-products'); ?>" />
                                    <br />
									<?PHP wc_tcsd_coupon_menu( __('Multi Discount Code', 'wc_tcsd'), 'wc-tcsd-google-plus-code'); ?>
                            	</div>
                                <div class="description"><?php _e('Enable and configure a Google Plus button. Note you can set different URLs for product pages and checkout / cart pages. You can also select a coupon to use with the Plus Button in Multi Discount Mode.', 'wc_tcsd') ?></div>
                            </div>
                        </div>
                        
                    </div>
                    
                    
                    <div id="twitter_options" class="options_page hide"> 
                    

                        <div class="option">
                            <h3><?php _e('Twitter API Locale', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                    <input class="textfield" name="wc-tcsd-twitter-api-locale" type="text" id="wc-tcsd-twitter-api-locale" value="<?php echo get_option('wc-tcsd-twitter-api-locale'); ?>" />
                            	</div>
                                <div class="description"><?php _e('Here you can set a custom language code to be used with the Twitter API / buttons. If not set the plugin will default to en for English.', 'wc_tcsd') ?></div>
                            </div>
                        </div>


                        <div class="option">
                            <h3><?php _e('Follow Button Setup', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                	<div class="tc-checkbox-wrap-wide">
                                    <label><input name="wc-tcsd-twitter-enabled" type="checkbox" class="tc-checkbox" id="wc-tcsd-twitter-enabled" value="true" <?PHP if( get_option('wc-tcsd-twitter-enabled') == 'true' ) : ?>checked="checked"<?PHP endif; ?> /><?PHP _e('Enable Follow Button', 'wc_tcsd'); ?></label><br />
                                    </div>
                                    <br />
									<label><?PHP _e('Twitter Username', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-twitter-username" type="text" id="wc-tcsd-twitter-username" value="<?php echo get_option('wc-tcsd-twitter-username'); ?>" />
                                    <br />
                                    <?PHP wc_tcsd_coupon_menu( __('Multi Discount Code', 'wc_tcsd'), 'wc-tcsd-twitter-follow-code'); ?>
                            	</div>
                                <div class="description"><?php _e('Here you can enable the Follow button and enter the Twitter username you want the Follow button to use.', 'wc_tcsd') ?></div>
                            </div>
                        </div>


                        <div class="option">
                            <h3><?php _e('Tweet Button Setup', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                	<div class="tc-checkbox-wrap-wide">
                                    <label><input name="wc-tcsd-twitter-tweet-enabled" type="checkbox" class="tc-checkbox" id="wc-tcsd-twitter-tweet-enabled" value="true" <?PHP if( get_option('wc-tcsd-twitter-tweet-enabled') == 'true' ) : ?>checked="checked"<?PHP endif; ?> /><?PHP _e('Enable Tweet Button', 'wc_tcsd'); ?></label><br />
                                    </div>
                                    <br />
									<label><?PHP _e('Tweet Button URL', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-twitter-url" type="text" id="wc-tcsd-twitter-url" value="<?php echo get_option('wc-tcsd-twitter-url'); ?>" />
                                    <br />
									<label><?PHP _e('Tweet Button URL (Product Pages)', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-twitter-url-products" type="text" id="wc-tcsd-twitter-url-products" value="<?php echo get_option('wc-tcsd-twitter-url-products'); ?>" />
                                    <br />
									<label><?PHP _e('Twitter Counter URL', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-twitter-counter" type="text" id="wc-tcsd-twitter-counter" value="<?php echo get_option('wc-tcsd-twitter-counter'); ?>" />
                                    <br />
									<label><?PHP _e('Tweet Button Text', 'wc_tcsd'); ?></label>
                                    <textarea class="textfield" name="wc-tcsd-twitter-tweet-text" cols="" rows="5" id="wc-tcsd-twitter-tweet-text"><?php echo str_replace('\\', "", get_option('wc-tcsd-twitter-tweet-text')); ?></textarea>
                                    <br />
                                    <?PHP wc_tcsd_coupon_menu( __('Multi Discount Code', 'wc_tcsd'), 'wc-tcsd-twitter-tweet-code'); ?>
                            	</div>
                                <div class="description"><?php _e('Here you can enable and set the URLs for the Tweet button that appears on product, checkout, and cart pages. You can also set the Twitter Counter URL, (optional) this URL will receive the count increase when a Tweet is made. If not set, the URL used in the button will get the count increase.', 'wc_tcsd') ?><br /><br /><?PHP _e('You can also enter pre-defined text that will appear in the Tweet box when the Tweet button is clicked.', 'wc_tcsd'); ?></div>
                            </div>
                        </div>
                                                                        
                    </div>   
                    
                    
                    <div id="linkedin_options" class="options_page hide">
                    
                        <div class="option">
                            <h3><?php _e('LinkedIn API Locale', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                    <input class="textfield" name="wc-tcsd-linkedin-api-locale" type="text" id="wc-tcsd-linkedin-api-locale" value="<?php echo get_option('wc-tcsd-linkedin-api-locale'); ?>" />
                            	</div>
                                <div class="description"><?php _e('Here you can enter a custom locale / language code to use with the LinkedIn JS API used for the Share button.', 'wc_tcsd') ?></div>
                            </div>
                        </div>
                        
                        <div class="option">
                            <h3><?php _e('LinkedIn Button Setup', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                	<div class="tc-checkbox-wrap-wide">
                                    <label><input name="wc-tcsd-linkedin-enabled" type="checkbox" class="tc-checkbox" id="wc-tcsd-linkedin-enabled" value="true" <?PHP if( get_option('wc-tcsd-linkedin-enabled') == 'true' ) : ?>checked="checked"<?PHP endif; ?> /><?PHP _e('Enable LinkedIn Button', 'wc_tcsd'); ?></label><br />
                                    </div>
                                    <br />
									<label><?PHP _e('LinkedIn Button URL', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-linkedin-url" type="text" id="wc-tcsd-linkedin-url" value="<?php echo get_option('wc-tcsd-linkedin-url'); ?>" />
                                    <br />
									<label><?PHP _e('LinkedIn Button URL (Product Pages)', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-linkedin-url-products" type="text" id="wc-tcsd-linkedin-url-products" value="<?php echo get_option('wc-tcsd-linkedin-url-products'); ?>" />
                                    <br />
									<?PHP wc_tcsd_coupon_menu( __('Multi Discount Code', 'wc_tcsd'), 'wc-tcsd-linkedin-code'); ?>
                            	</div>
                                <div class="description"><?php _e('Enable and configure a LinkedIn Share button. Note you can set different URLs for product pages and checkout / cart pages. You can also select a coupon to use with the Share Button in Multi Discount Mode.', 'wc_tcsd') ?></div>
                            </div>
                        </div>
                        
                    </div>

                    <div id="vk_options" class="options_page hide">
                    
                        <div class="option">
                            <h3><?php _e('Vkontakte App ID', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                    <input class="textfield" name="wc-tcsd-vk-app-id" type="text" id="wc-tcsd-vk-app-id" value="<?php echo get_option('wc-tcsd-vk-app-id'); ?>" />
                            	</div>
                                <div class="description"><?php _e('Enter the App ID from VK here that you created for your site / domain.', 'wc_tcsd') ?></div>
                            </div>
                        </div>

                        <div class="option">
                            <h3><?php _e('Vkontakte API Locale', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                    <input class="textfield" name="wc-tcsd-vk-api-locale" type="text" id="wc-tcsd-vk-api-locale" value="<?php echo get_option('wc-tcsd-vk-api-locale'); ?>" />
                            	</div>
                                <div class="description"><?php _e('Here you can enter a custom locale / language code to use with the VK JS API used for the Like button.', 'wc_tcsd') ?> <a href="https://developers.google.com/+/web/api/supported-languages" target="_blank"><?PHP _e('Supported Languages', 'wc_tcsd'); ?></a></div>
                            </div>
                        </div>
                        
                        <div class="option">
                            <h3><?php _e('VK Like Button Setup', 'wc_tcsd') ?></h3>
                            <div class="section">
                            	<div class="element">
                                	<div class="tc-checkbox-wrap-wide">
                                    <label><input name="wc-tcsd-vk-enabled" type="checkbox" class="tc-checkbox" id="wc-tcsd-vk-enabled" value="true" <?PHP if( get_option('wc-tcsd-vk-enabled') == 'true' ) : ?>checked="checked"<?PHP endif; ?> /><?PHP _e('Enable VK Like Button', 'wc_tcsd'); ?></label><br />
                                    </div>
                                    <br />
									<label><?PHP _e('Like Button URL', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-vk-url" type="text" id="wc-tcsd-vk-url" value="<?php echo get_option('wc-tcsd-vk-url'); ?>" />
                                    <br />
									<label><?PHP _e('Like Button URL (Product Pages)', 'wc_tcsd'); ?></label>
                                    <input class="textfield" name="wc-tcsd-vk-url-products" type="text" id="wc-tcsd-linkedin-url-products" value="<?php echo get_option('wc-tcsd-vk-url-products'); ?>" />
                                    <br />
									<?PHP wc_tcsd_coupon_menu( __('Multi Discount Code', 'wc_tcsd'), 'wc-tcsd-vk-code'); ?>
                            	</div>
                                <div class="description"><?php _e('Enable and configure a VK Like button. Note you can set different URLs for product pages and checkout / cart pages. You can also select a coupon to use with the Like Button in Multi Discount Mode.', 'wc_tcsd') ?></div>
                            </div>
                        </div>
                                                
                    </div>
                                        
            		<br class="clear" />
                    
            </div>
            
            <div class="info bottom">
            
                <input type="submit" name="settingsBtn" id="settingsBtn" class="button-framework save-options" value="<?php _e('Save All Changes', 'wc_tcsd') ?>" />
            
            </div>
            
        </form>
        
    </div>

</div>

<?php } ?>