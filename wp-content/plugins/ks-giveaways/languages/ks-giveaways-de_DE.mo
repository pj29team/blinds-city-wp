��    -      �  =   �      �     �  	   �       G   $  7   l     �     �     �     �  
   �     �  3   �  
   (     3     B     P  I   `  2   �  %   �       b     E   v     �  0   �     �          /     E     W     _  +   |     �     �  #   �      �          (     >     V     j     {  	   �     	  #   	  �  7	      �
     �
     �
  `   �
  Y   V  "   �  %   �     �            
     C   &     j     r     �     �  V   �  A     0   J     {  h   �  N   �     I  >   b  "   �     �     �     �  
          /   4     d     w  (   �  #   �     �     �       !   '     I  x   c  
   �     �  -   �                     ,                 !                     "      (          
       )      #                 $      &   '                        	      -                  +                               %      *    An email address is required Anonymous Answer correctly to qualify Cannot publish giveaway because provided image link URLs are not valid. Cannot publish giveaway until all fields are completed. Custom field deleted. Custom field updated. Did you mean Email Embed Code Enter Enter sweepstakes and receive exclusive offers from First Name Giveaway Ended Giveaway Ends Giveaway Starts Giveaway draft updated. <a target="_blank" href="%s">Preview giveaway</a> Giveaway published. <a href="%s">View giveaway</a> Giveaway restored to revision from %s Giveaway saved. Giveaway scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview giveaway</a> Giveaway submitted. <a target="_blank" href="%s">Preview giveaway</a> Giveaway updated. Giveaway updated. <a href="%s">View giveaway</a> Incorrect answer, try again! KingSumo Giveaways No contestants found. No winners found. Pending Please enter your first name Powered by KingSumo Giveaways for WordPress Prizes Awarded Read official rules Step 1 &mdash; Giveaway Information Step 2 &mdash; Prize Information Step 3 &mdash; Question Step 4 &mdash; Design Step 5 &mdash; Services Unsubscribe anytime Verifying Winner Your number of contest entries will display once you have confirmed your email address. Please check your inbox to confirm now. confirmed email address is not affiliated with the giveaway Project-Id-Version: 
POT-Creation-Date: 2017-03-03 10:23-0800
PO-Revision-Date: 2017-06-02 00:07-0700
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
Last-Translator: 
Language: de_DE
X-Poedit-SearchPath-0: .
 Wir brauchen eine E-mail Adresse Anonym Frage beantworten Gewinnspiel kann nicht veröffentlicht werden da die angegebenen Bild URLs nicht verfügbar sind Gewinnspiel kann nicht veröffentlicht werden solange nicht alle Felder ausgefüllt sind. Benutzerdefiniertes Feld gelöscht Benutzerdefiniertes Feld aktualisiert Meintest du E-mail Code einfügen Teilnehmen Nimm an unserem Gewinnspiel teil und erhalte exklusive Angebote von Vorname Gewinnspiel vorbei Gewinnspiel endet am Gewinnspiel startet am Gewinnspielentwurf aktualisiert. <a target="_blank" href="%s">Vorschau Gewinnspiel</a> Gewinnspiel veröffentlicht. <a href="%s">Gewinnspiel ansehen</a> Gewinnspiel wiederhergestellt zur Version vom %s Gewinnspiel gesichert Gewinnspiel geplant für: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Vorschau Gewinnspiel</a> Gewinnspiel eingereicht. <a target="_blank" href="%s">Vorschau Gewinnspiel</a> Gewinnspiel aktualisiert Gewinnspiel aktualisiert. <a href="%s">Gewinnspiel ansehen</a> Leider falsch, versuch es nochmal! KingSumo Gewinnspiel Keine Konstante gefunden. Kein Gewinner gefunden. Ausstehend Bitte gib deinen Vornamen ein Powered by KingSumo Gewinnspiele für WordPress Gewinner ausgelost Offizielle Regeln lesen Schritt 1 &mdash; Gewinnspielinformation Schritt 2 &mdash; Preis Information Schritt 3 &mdash; Frage Schritt 4 &mdash; Design Schritt 5 &mdash; Dienste Du kannst dich jederzeit abmelden Gewinner wird verifiziert Die Anzahl deiner Lose wird angezeigt, sobald du deine Email Adressebestätigt hast. Bitte schau in deiner Mailbox nach. bestätigt E-mail steht nicht in Verbindung mit dem Gewinnspiel 