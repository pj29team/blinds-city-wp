<?php
/*
Plugin Name: WooCommerce Sample Products
Plugin Ver: 1.0.0
Description: Add your sample products.
Author: Ivan Nikolayenko
*/

if (!defined('ABSPATH')) {
    exit;
}

class SampleProducts {
    private static $_instance;
    public static $plugin_dir;
    public static $plugin_dir_uri;
    public static $template_dir;
    public static $template_dir_uri;
	public static $domain = 'wc-sample-products';
    
    public function generalProductData() {
        echo '<div class="options_group">';
        
		woocommerce_wp_checkbox(array(
			'id'	=> '_product_is_sample',
			'label'	=> __('Is sample', self::$domain)
		));
        
        echo '</div>';
    }
	
	public function saveProduct($post_id) {
		if (!($the_product instanceof WC_Product)) {
			return;
		}
		
		$_product_is_sample = isset($_POST['_product_is_sample']) ? 'yes' : 'no';
		
		update_post_meta($post_id, '_product_is_sample', $_product_is_sample);
	}
	
	public function productFilterPrice($price, $product) {
		if ($product->product_is_sample === 'yes') {
			return 0;
		}
		
		return $price;
	}
	
	public function productFilterTitle($title, $product) {
		if ($product->product_is_sample === 'yes') {
			return $title . ' Sample';
		}
		
		return $title;
	}
	
	public function postFilterTitle($title, $post_id) {
		if (get_post_meta($post_id, '_product_is_sample', true) === 'yes') {
			return $title . ' Sample';
		}
		
		return $title;
	}
    
    private function __construct() {
		self::$plugin_dir		= dirname(__FILE__);
		self::$plugin_dir_uri	= plugin_dir_url(__FILE__);
		self::$template_dir		= get_template_directory();
		self::$template_dir_uri	= get_template_directory_uri();
		
		load_plugin_textdomain(self::$domain, false, self::$plugin_dir . '/languages');
        
        add_action('woocommerce_product_options_general_product_data', array($this, 'generalProductData'));
        add_action('woocommerce_process_product_meta', array($this, 'saveProduct'));
		add_filter('woocommerce_product_title', array($this, 'productFilterTitle'), 10, 2);
		add_filter('woocommerce_get_price', array($this, 'productFilterPrice'), 10, 2);
		add_filter('the_title', array($this, 'postFilterTitle'), 10, 2);
    }
    
    protected function __clone() {
        
    }
    
    public static function getInstance() {
        if (!self::$_instance) {
            self::$_instance = new SampleProducts;
        }
        return self::$_instance;
    }
}

function SampleProducts() {
    return SampleProducts::getInstance();
}

$GLOBALS['SampleProducts'] = SampleProducts();
